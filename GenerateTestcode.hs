{-# LANGUAGE MagicHash #-}

module GenerateTestcode where

import GHC.Exts
import GHC.Prim
import GHC.Exts
import GHC.Prim
import GHC.Stack
import Data.Bits
import GHC.Float
import GHC.Word
import GHC.Int
import Data.Int
import Data.List
import System.Exit
import Data.Binary.Get
import Data.Word
import GHC.Float
import Numeric.Natural

import Debug.Trace

import qualified Data.ByteString.Lazy as BL

import OperationCode


word64ToInt64 (W64# x) = I64# (word64ToInt64# x)



data TestCase
  = TestCase
      {-# UNPACK #-} !Word8
      {-# UNPACK #-} !Word64
      {-# UNPACK #-} !Word64
      {-# UNPACK #-} !Word64
  deriving (Show)


getTestCase :: Get TestCase
getTestCase = do
  operator_id <- getWord8

  let signature = typeSignature operator_id

  (output, left, right) <- getForTypeSignature signature

  return $! TestCase
    operator_id
    output
    left
    right

getForTypeSignature
  :: TypeSignature
  -> Get (Word64, Word64, Word64)
getForTypeSignature
  (Unary (To to) (From from))
  = do
  to_number <- getForType to
  from_number <- getForType from
  return (to_number, from_number, 0)
getForTypeSignature
  (Binary (To to) (From left) (From right))
  = do
  to_number <- getForType to
  left_number <- getForType left
  right_number <- getForType right
  return (to_number, left_number, right_number)



getForType
  :: Type
  -> Get Word64
getForType I32 = fmap fromIntegral getWord32le
getForType I64 = getWord64le
getForType F32 = fmap fromIntegral getWord32le
getForType F64 = getWord64le






getTestCases :: Get [TestCase]
getTestCases = do
  empty <- isEmpty
  if empty
    then return []
    else do trade <- getTestCase
            trades <- getTestCases
            return (trade:trades)







lazyIOExample :: IO [TestCase]
lazyIOExample = do
--   input <- BL.readFile "raw-testdata-f64.bin"
--   input <- BL.readFile "raw_testdata_with_duging.bin"
--   input <- BL.readFile "raw_testdata_nan.bin"

  input <- BL.readFile "raw_test_newopcode_testing.bin"

  return (runGet getTestCases input)


visualizeTestCases :: IO ()
visualizeTestCases = do
  examples <- lazyIOExample

  let lines = map showVisualuze2 examples

  putStrLn $ unlines $ take 10 lines

  putStrLn "..."
  where
    showVisualuze2
      (TestCase
        operator_id
        supposed_to_be_output
        left_argument
        right_argument
      )
      = ""
      ++ show operator_id
      ++ " "
      ++ show supposed_to_be_output
      ++ " "
      ++ show left_argument
      ++ " "
      ++ show right_argument





-- TODO this assumes, that unary operators have a 0 as second operator
filterOutTwoNaNCases2 = filter hasUnwantedNaNs
  where
    hasUnwantedNaNs
      (TestCase
        operator_id
        supposed_to_be_output
        left_argument
        right_argument
      )
      = not
      $ case typeSignature operator_id of
          Unary _ (From I32) -> False
          Unary _ (From I64) -> False
          Unary _ (From F32) -> False
          Unary _ (From F64) -> False
          Binary _ (From F32) (From F32)
            -> isNaN (castWord32ToFloat $ fromInteger $ toInteger left_argument)
            && isNaN (castWord32ToFloat $ fromInteger $ toInteger right_argument)
          Binary _ (From F64) (From F64)
            -> isNaN (castWord64ToDouble left_argument)
            && isNaN (castWord64ToDouble right_argument)
          x -> error $ show x



generateShellScript :: IO ()
generateShellScript = do
  examples <- lazyIOExample

  let lines
        = id
--         $ map head
--         $ group
        $ map showVisualuze
--         $ filterOutTwoNaNCases2
        $ examples

  let numberOfLines = length lines

  let echos = id
        $ map ("echo "++)
        $ map show
        $ map (\x -> numberOfLines - x)
        $ [0..]

  let anotations
        = (>>= \(x,y)->[x,y])
        $ zip echos
        $ lines

  writeFile "automatic-testing.sh" $ unlines $ anotations

  where
--     wasmtime RoundingFiasco.wasm --invoke testcase $f32_demote_f64 $ceil 1 1

    showVisualuze
      (TestCase
        operator_id
        supposed_to_be_output
        left_argument
        right_argument
      )
      = ""
      ++ "wasmtime RoundingFiasco.wasm --invoke testcase"
      ++ " "
      ++ "$"
      ++ showOpCode operator_id
      ++ " "
      ++ " "
      ++ showForWasmtime supposed_to_be_output
      ++ " "
      ++ showForWasmtime (left_argument)
      ++ " "
      ++ showForWasmtime right_argument

    showForWasmtime = show . word64ToInt64






generateWebassemblyTestWatModule :: IO ()
generateWebassemblyTestWatModule
  = do
  examples <- lazyIOExample

  let cases
        = id
        $ (\x -> zipWith ($) x [0..])
        $ map prepare
--         $ take 8000
--         $ drop (1000 * 2)
--         $ take 1
--         $ drop 49
--         $ filterOutTwoNaNCases2
        $ examples
        :: [TestTriple]


  let final_result
        testcases
        = []
        ++ ["(module                                                "]
        ++ ["                                                       "]
        ++ ["  (import \"host\" \"print\" (func $i64.print (param i64)))"]
        ++ ["  (import \"host\" \"print\" (func $f64.print (param f64)))"]
        ++ ["  (import \"host\" \"print\" (func $i32.print (param i32)))"]
        ++ ["  (import \"host\" \"print\" (func $f32.print (param f32)))"]
        ++ ["                                                       "]
        ++ ["  (export \"TESING\" (func $TESING))                   "]
        ++ ["                                                       "]
        ++ ["  (func                                                "]
        ++ ["    $TESING                                            "]
        ++ testcases
        ++ ["                                                       "]
        ++ ["  )                                                    "]
        ++ [")                                                      "]

  let content = unlines $ final_result $ (cases >>= oneTestcase)

--   putStrLn $ content
  writeFile "../compiling-wabt/compiling-wabt/processing/source/testsuite.wat" $ content

  where
    prepare
      testcae@(TestCase
        operator_id
        output
        left
        right
      )
      testcaseNumber
      = TestTriple
          testcaseNumber
          testcae
          (typeSignature operator_id)



-- TODO reafactor: use only better testcase?
data TestTriple
  = TestTriple
      Natural
      TestCase
      TypeSignature
  deriving Show



oneTestcase
  :: TestTriple
  -> [String]

-- oneTestcase
--   x
--   = error
--   $ "oneTestcase: "
--   ++ show x

oneTestcase
  (TestTriple
    testcaseNumber
    (TestCase
      operator_id
      output
      left
      right
    )
    typeSignature
  )
  = []
  ++ ["                                                       "]
  ++ ["    (if                                                "]
  ++ ["      ("++toRawType++".eq                                          "]
  ++ ["        ("++toRawType++".const "++show output++")                  "]
  ++ expression
  ++ ["      )                                                "]
  ++ ["      (then                                            "]
  ++ ["        ;; testcase pased                              "]
  ++ ["      )                                                "]
  ++ ["      (else                                            "]
  ++ ["        ;; testcase failed                             "]
  ++ ["        ;; called host host.print(i64:"++show testcaseNumber++")"]
  ++ ["        (call $i64.print                               "]
  ++ ["          (i64.const "++show testcaseNumber++")        "]
  ++ ["        )                                              "]
  ++ ["        (call $"++toRawType++".print                               "]
  ++ (map ("  "++) expression)
  ++ ["        )                                              "]
  ++ ["        unreachable                                    "]
  ++ ["      )                                                "]
  ++ ["    )                                                  "]
  where
    expression = []
      ++ ["        ("++toRawType++".reinterpret_"++toType++"                           "]
      ++ ["          ("++name_of_operator++"                      "]
      ++ first_argument
      ++ maybeSecondParameter
      ++ ["          )                                            "]
      ++ ["        )                                              "]

    name_of_operator = syntacWat operator_id

    showType F32 = "f32"
    showType F64 = "f64"
    showType I32 = "i32"
    showType I64 = "i64"

    toTypeCateory F32 = showType I32
    toTypeCateory F64 = showType I64

    fromRawType = toTypeCateory from
    fromType = showType from

    toRawType
      = case typeSignature of
          Unary (To to) _ -> toTypeCateory to
          Binary (To to) _ _ -> toTypeCateory to

    toType
      = case typeSignature of
          Unary (To to) _ -> showType to
          Binary (To to) _ _ -> showType to

    from
      = case typeSignature of
          Unary _ (From x) -> x
          Binary _ (From x) _ -> x

    maybeSecondParameter
      = case typeSignature of
          Binary _ _ (From _)
            ->  []
                ++ ["            ("++fromType++".reinterpret_"++fromRawType++"                       "]
                ++ ["              ("++fromRawType++".const "++show right++")              "]
                ++ ["            )                                          "]
          _ -> []

    first_argument
      = case typeSignature of
          Binary _ _ _ -> first_argument_from_floating
          Unary _ (From F32) -> first_argument_from_floating
          Unary _ (From F64) -> first_argument_from_floating
          Unary _ (From i)
            -> ["            ("++(showType i)++".const "++show left++")              "]

    first_argument_from_floating = []
      ++ ["            ("++fromType++".reinterpret_"++fromRawType++"                       "]
      ++ ["              ("++fromRawType++".const "++show left++")              "]
      ++ ["            )                                          "]



