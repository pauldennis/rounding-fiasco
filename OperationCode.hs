module OperationCode where

import Data.Word
import GHC.Stack


showOpCode 0x1C = "f32_binary_sign        "
showOpCode 0x1D = "i32_trinary_sign_f32   "
showOpCode 0x1E = "f64_binary_sign        "
showOpCode 0x1F = "i32_trinary_sign_f64   "
showOpCode 0x20 = "f32_sqrt_ceil          "
showOpCode 0x21 = "f32_add_ceil           "
showOpCode 0x22 = "f32_sub_ceil           "
showOpCode 0x23 = "f32_mul_ceil           "
showOpCode 0x24 = "f32_div_ceil           "
showOpCode 0x25 = "f64_sqrt_ceil          "
showOpCode 0x26 = "f64_add_ceil           "
showOpCode 0x27 = "f64_sub_ceil           "
showOpCode 0x28 = "f64_mul_ceil           "
showOpCode 0x29 = "f64_div_ceil           "
showOpCode 0x32 = "f32_convert_i32_s_ceil "
showOpCode 0x33 = "f32_convert_i32_u_ceil "
showOpCode 0x34 = "f32_convert_i64_s_ceil "
showOpCode 0x35 = "f32_convert_i64_u_ceil "
showOpCode 0x36 = "f32_demote_f64_ceil    "
showOpCode 0x37 = "f64_convert_i32_s_ceil "
showOpCode 0x38 = "f64_convert_i32_u_ceil "
showOpCode 0x39 = "f64_convert_i64_s_ceil "
showOpCode 0x3A = "f64_convert_i64_u_ceil "
showOpCode 0x3B = "f64_promote_f32_ceil   "
showOpCode 0x40 = "f32_sqrt_floor         "
showOpCode 0x41 = "f32_add_floor          "
showOpCode 0x42 = "f32_sub_floor          "
showOpCode 0x43 = "f32_mul_floor          "
showOpCode 0x44 = "f32_div_floor          "
showOpCode 0x45 = "f64_sqrt_floor         "
showOpCode 0x46 = "f64_add_floor          "
showOpCode 0x47 = "f64_sub_floor          "
showOpCode 0x48 = "f64_mul_floor          "
showOpCode 0x49 = "f64_div_floor          "
showOpCode 0x52 = "f32_convert_i32_s_floor"
showOpCode 0x53 = "f32_convert_i32_u_floor"
showOpCode 0x54 = "f32_convert_i64_s_floor"
showOpCode 0x55 = "f32_convert_i64_u_floor"
showOpCode 0x56 = "f32_demote_f64_floor   "
showOpCode 0x57 = "f64_convert_i32_s_floor"
showOpCode 0x58 = "f64_convert_i32_u_floor"
showOpCode 0x59 = "f64_convert_i64_s_floor"
showOpCode 0x5A = "f64_convert_i64_u_floor"
showOpCode 0x5B = "f64_promote_f32_floor  "
showOpCode 0x60 = "f32_sqrt_trunc         "
showOpCode 0x61 = "f32_add_trunc          "
showOpCode 0x62 = "f32_sub_trunc          "
showOpCode 0x63 = "f32_mul_trunc          "
showOpCode 0x64 = "f32_div_trunc          "
showOpCode 0x65 = "f64_sqrt_trunc         "
showOpCode 0x66 = "f64_add_trunc          "
showOpCode 0x67 = "f64_sub_trunc          "
showOpCode 0x68 = "f64_mul_trunc          "
showOpCode 0x69 = "f64_div_trunc          "
showOpCode 0x72 = "f32_convert_i32_s_trunc"
showOpCode 0x73 = "f32_convert_i32_u_trunc"
showOpCode 0x74 = "f32_convert_i64_s_trunc"
showOpCode 0x75 = "f32_convert_i64_u_trunc"
showOpCode 0x76 = "f32_demote_f64_trunc   "
showOpCode 0x77 = "f64_convert_i32_s_trunc"
showOpCode 0x78 = "f64_convert_i32_u_trunc"
showOpCode 0x79 = "f64_convert_i64_s_trunc"
showOpCode 0x7A = "f64_convert_i64_u_trunc"
showOpCode 0x7B = "f64_promote_f32_trunc  "








data Type = I32 | I64 | F32 | F64
  deriving Show

newtype To = To Type
  deriving Show
newtype From = From Type
  deriving Show

data TypeSignature
  = Unary
      To
      From
  | Binary
      To
      From
      From
  deriving Show


typeSignature
  :: HasCallStack
  => Word8
  -> TypeSignature

typeSignature 0x1C = Unary   (To I32)    (From F32)                                 -- f32.binary_sign
typeSignature 0x1D = Unary   (To I32)    (From F32)                                 -- i32.trinary_sign_f32
typeSignature 0x1E = Unary   (To I32)    (From F64)                                 -- f64.binary_sign
typeSignature 0x1F = Unary   (To I32)    (From F64)                                 -- i32.trinary_sign_f64
typeSignature 0x20 = Unary   (To F32)    (From F32)                                 -- f32.sqrt_ceil
typeSignature 0x21 = Binary  (To F32)    (From F32)     (From F32)                  -- f32.add_ceil
typeSignature 0x22 = Binary  (To F32)    (From F32)     (From F32)                  -- f32.sub_ceil
typeSignature 0x23 = Binary  (To F32)    (From F32)     (From F32)                  -- f32.mul_ceil
typeSignature 0x24 = Binary  (To F32)    (From F32)     (From F32)                  -- f32.div_ceil
typeSignature 0x25 = Unary   (To F64)    (From F64)                                 -- f64.sqrt_ceil
typeSignature 0x26 = Binary  (To F64)    (From F64)     (From F64)                  -- f64.add_ceil
typeSignature 0x27 = Binary  (To F64)    (From F64)     (From F64)                  -- f64.sub_ceil
typeSignature 0x28 = Binary  (To F64)    (From F64)     (From F64)                  -- f64.mul_ceil
typeSignature 0x29 = Binary  (To F64)    (From F64)     (From F64)                  -- f64.div_ceil
typeSignature 0x32 = Unary   (To F32)    (From I32)                                 -- f32.convert_i32_s_ceil
typeSignature 0x33 = Unary   (To F32)    (From I32)                                 -- f32.convert_i32_u_ceil
typeSignature 0x34 = Unary   (To F32)    (From I64)                                 -- f32.convert_i64_s_ceil
typeSignature 0x35 = Unary   (To F32)    (From I64)                                 -- f32.convert_i64_u_ceil
typeSignature 0x36 = Unary   (To F32)    (From F64)                                 -- f32.demote_f64_ceil
typeSignature 0x37 = Unary   (To F64)    (From I32)                                 -- f64.convert_i32_s_ceil
typeSignature 0x38 = Unary   (To F64)    (From I32)                                 -- f64.convert_i32_u_ceil
typeSignature 0x39 = Unary   (To F64)    (From I64)                                 -- f64.convert_i64_s_ceil
typeSignature 0x3A = Unary   (To F64)    (From I64)                                 -- f64.convert_i64_u_ceil
typeSignature 0x3B = Unary   (To F64)    (From F32)                                 -- f64.promote_f32_ceil
typeSignature 0x40 = Unary   (To F32)    (From F32)                                 -- f32.sqrt_floor
typeSignature 0x41 = Binary  (To F32)    (From F32)     (From F32)                  -- f32.add_floor
typeSignature 0x42 = Binary  (To F32)    (From F32)     (From F32)                  -- f32.sub_floor
typeSignature 0x43 = Binary  (To F32)    (From F32)     (From F32)                  -- f32.mul_floor
typeSignature 0x44 = Binary  (To F32)    (From F32)     (From F32)                  -- f32.div_floor
typeSignature 0x45 = Unary   (To F64)    (From F64)                                 -- f64.sqrt_floor
typeSignature 0x46 = Binary  (To F64)    (From F64)     (From F64)                  -- f64.add_floor
typeSignature 0x47 = Binary  (To F64)    (From F64)     (From F64)                  -- f64.sub_floor
typeSignature 0x48 = Binary  (To F64)    (From F64)     (From F64)                  -- f64.mul_floor
typeSignature 0x49 = Binary  (To F64)    (From F64)     (From F64)                  -- f64.div_floor
typeSignature 0x52 = Unary   (To F32)    (From I32)                                 -- f32.convert_i32_s_floor
typeSignature 0x53 = Unary   (To F32)    (From I32)                                 -- f32.convert_i32_u_floor
typeSignature 0x54 = Unary   (To F32)    (From I64)                                 -- f32.convert_i64_s_floor
typeSignature 0x55 = Unary   (To F32)    (From I64)                                 -- f32.convert_i64_u_floor
typeSignature 0x56 = Unary   (To F32)    (From F64)                                 -- f32.demote_f64_floor
typeSignature 0x57 = Unary   (To F64)    (From I32)                                 -- f64.convert_i32_s_floor
typeSignature 0x58 = Unary   (To F64)    (From I32)                                 -- f64.convert_i32_u_floor
typeSignature 0x59 = Unary   (To F64)    (From I64)                                 -- f64.convert_i64_s_floor
typeSignature 0x5A = Unary   (To F64)    (From I64)                                 -- f64.convert_i64_u_floor
typeSignature 0x5B = Unary   (To F64)    (From F32)                                 -- f64.promote_f32_floor
typeSignature 0x60 = Unary   (To F32)    (From F32)                                 -- f32.sqrt_trunc
typeSignature 0x61 = Binary  (To F32)    (From F32)     (From F32)                  -- f32.add_trunc
typeSignature 0x62 = Binary  (To F32)    (From F32)     (From F32)                  -- f32.sub_trunc
typeSignature 0x63 = Binary  (To F32)    (From F32)     (From F32)                  -- f32.mul_trunc
typeSignature 0x64 = Binary  (To F32)    (From F32)     (From F32)                  -- f32.div_trunc
typeSignature 0x65 = Unary   (To F64)    (From F64)                                 -- f64.sqrt_trunc
typeSignature 0x66 = Binary  (To F64)    (From F64)     (From F64)                  -- f64.add_trunc
typeSignature 0x67 = Binary  (To F64)    (From F64)     (From F64)                  -- f64.sub_trunc
typeSignature 0x68 = Binary  (To F64)    (From F64)     (From F64)                  -- f64.mul_trunc
typeSignature 0x69 = Binary  (To F64)    (From F64)     (From F64)                  -- f64.div_trunc
typeSignature 0x72 = Unary   (To F32)    (From I32)                                 -- f32.convert_i32_s_trunc
typeSignature 0x73 = Unary   (To F32)    (From I32)                                 -- f32.convert_i32_u_trunc
typeSignature 0x74 = Unary   (To F32)    (From I64)                                 -- f32.convert_i64_s_trunc
typeSignature 0x75 = Unary   (To F32)    (From I64)                                 -- f32.convert_i64_u_trunc
typeSignature 0x76 = Unary   (To F32)    (From F64)                                 -- f32.demote_f64_trunc
typeSignature 0x77 = Unary   (To F64)    (From I32)                                 -- f64.convert_i32_s_trunc
typeSignature 0x78 = Unary   (To F64)    (From I32)                                 -- f64.convert_i32_u_trunc
typeSignature 0x79 = Unary   (To F64)    (From I64)                                 -- f64.convert_i64_s_trunc
typeSignature 0x7A = Unary   (To F64)    (From I64)                                 -- f64.convert_i64_u_trunc
typeSignature 0x7B = Unary   (To F64)    (From F32)                                 -- f64.promote_f32_trunc


typeSignature x = error $ "unknown code: " ++ show x



syntacWat 0x1C = "f32.binary_sign        "
syntacWat 0x1D = "i32.trinary_sign_f32   "
syntacWat 0x1E = "f64.binary_sign        "
syntacWat 0x1F = "i32.trinary_sign_f64   "
syntacWat 0x20 = "f32.sqrt_ceil          "
syntacWat 0x21 = "f32.add_ceil           "
syntacWat 0x22 = "f32.sub_ceil           "
syntacWat 0x23 = "f32.mul_ceil           "
syntacWat 0x24 = "f32.div_ceil           "
syntacWat 0x25 = "f64.sqrt_ceil          "
syntacWat 0x26 = "f64.add_ceil           "
syntacWat 0x27 = "f64.sub_ceil           "
syntacWat 0x28 = "f64.mul_ceil           "
syntacWat 0x29 = "f64.div_ceil           "
syntacWat 0x32 = "f32.convert_i32_s_ceil "
syntacWat 0x33 = "f32.convert_i32_u_ceil "
syntacWat 0x34 = "f32.convert_i64_s_ceil "
syntacWat 0x35 = "f32.convert_i64_u_ceil "
syntacWat 0x36 = "f32.demote_f64_ceil    "
syntacWat 0x37 = "f64.convert_i32_s_ceil "
syntacWat 0x38 = "f64.convert_i32_u_ceil "
syntacWat 0x39 = "f64.convert_i64_s_ceil "
syntacWat 0x3A = "f64.convert_i64_u_ceil "
syntacWat 0x3B = "f64.promote_f32_ceil   "
syntacWat 0x40 = "f32.sqrt_floor         "
syntacWat 0x41 = "f32.add_floor          "
syntacWat 0x42 = "f32.sub_floor          "
syntacWat 0x43 = "f32.mul_floor          "
syntacWat 0x44 = "f32.div_floor          "
syntacWat 0x45 = "f64.sqrt_floor         "
syntacWat 0x46 = "f64.add_floor          "
syntacWat 0x47 = "f64.sub_floor          "
syntacWat 0x48 = "f64.mul_floor          "
syntacWat 0x49 = "f64.div_floor          "
syntacWat 0x52 = "f32.convert_i32_s_floor"
syntacWat 0x53 = "f32.convert_i32_u_floor"
syntacWat 0x54 = "f32.convert_i64_s_floor"
syntacWat 0x55 = "f32.convert_i64_u_floor"
syntacWat 0x56 = "f32.demote_f64_floor   "
syntacWat 0x57 = "f64.convert_i32_s_floor"
syntacWat 0x58 = "f64.convert_i32_u_floor"
syntacWat 0x59 = "f64.convert_i64_s_floor"
syntacWat 0x5A = "f64.convert_i64_u_floor"
syntacWat 0x5B = "f64.promote_f32_floor  "
syntacWat 0x60 = "f32.sqrt_trunc         "
syntacWat 0x61 = "f32.add_trunc          "
syntacWat 0x62 = "f32.sub_trunc          "
syntacWat 0x63 = "f32.mul_trunc          "
syntacWat 0x64 = "f32.div_trunc          "
syntacWat 0x65 = "f64.sqrt_trunc         "
syntacWat 0x66 = "f64.add_trunc          "
syntacWat 0x67 = "f64.sub_trunc          "
syntacWat 0x68 = "f64.mul_trunc          "
syntacWat 0x69 = "f64.div_trunc          "
syntacWat 0x72 = "f32.convert_i32_s_trunc"
syntacWat 0x73 = "f32.convert_i32_u_trunc"
syntacWat 0x74 = "f32.convert_i64_s_trunc"
syntacWat 0x75 = "f32.convert_i64_u_trunc"
syntacWat 0x76 = "f32.demote_f64_trunc   "
syntacWat 0x77 = "f64.convert_i32_s_trunc"
syntacWat 0x78 = "f64.convert_i32_u_trunc"
syntacWat 0x79 = "f64.convert_i64_s_trunc"
syntacWat 0x7A = "f64.convert_i64_u_trunc"
syntacWat 0x7B = "f64.promote_f32_trunc  "


