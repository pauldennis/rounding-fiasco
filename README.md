# RoundingFiasco


This repository compiles a WebAssembly module for rounded instructions:
  - `squareRoot`
  - `+`, `-`, `*`, `/`
  - integer conversions
  - demotion from `double` to `single` precision

for the prevalent `single` and `double` precision floating point format.

## download
The WebAssembly module can be found here
[RoundingFiasco.wasm](RoundingFiasco.wasm).

## quirks

The library is aware about the following quirky behaviour of IEEE floating points:
  - `squareRoot (-0.0) == (-0.0)`
  - `biggest_finit_float +_ceil smallest_positiv_float == +Infinity`
  - `1.0 /_ceil (-0.0) == -Infinity` but not `== smallest_finit_float`
  - `123.0 +_not_floor (-123.0) == 0.0` but `123.0 +_floor (-123.0) == -0.0`
  - `123.0 -_not_floor 123.0 == 0.0` but `123.0 -_floor 123.0 == -0.0`
  - `x / 0 = +Infinity, 0 < x` and `0 / 0 = NaN`



## module export list

```
i32_sign_bit_f32
i32_arithmic_signum_f32
i32_sign_bit_f64
i32_arithmic_signum_f64
f32_squareRoot_ceil
f32_add_ceil
f32_subtract_ceil
f32_multiplicate_ceil
f32_divide_ceil
f64_squareRoot_ceil
f64_add_ceil
f64_subtract_ceil
f64_multiplicate_ceil
f64_divide_ceil
f32_convert_i32_signed_ceil
f32_convert_i32_unsigned_ceil
f32_convert_i64_signed_ceil
f32_convert_i64_unsigned_ceil
f32_demote_f64_ceil
f64_convert_i32_signed_ceil
f64_convert_i32_unsigned_ceil
f64_convert_i64_signed_ceil
f64_convert_i64_unsigned_ceil
f64_promote_f32_ceil
f32_squareRoot_floor
f32_add_floor
f32_subtract_floor
f32_multiplicate_floor
f32_divide_floor
f64_squareRoot_floor
f64_add_floor
f64_subtract_floor
f64_multiplicate_floor
f64_divide_floor
f32_convert_i32_signed_floor
f32_convert_i32_unsigned_floor
f32_convert_i64_signed_floor
f32_convert_i64_unsigned_floor
f32_demote_f64_floor
f64_convert_i32_signed_floor
f64_convert_i32_unsigned_floor
f64_convert_i64_signed_floor
f64_convert_i64_unsigned_floor
f64_promote_f32_floor
f32_squareRoot_truncate
f32_add_truncate
f32_subtract_truncate
f32_multiplicate_truncate
f32_divide_truncate
f64_squareRoot_truncate
f64_add_truncate
f64_subtract_truncate
f64_multiplicate_truncate
f64_divide_truncate
f32_convert_i32_signed_truncate
f32_convert_i32_unsigned_truncate
f32_convert_i64_signed_truncate
f32_convert_i64_unsigned_truncate
f32_demote_f64_truncate
f64_convert_i32_signed_truncate
f64_convert_i32_unsigned_truncate
f64_convert_i64_signed_truncate
f64_convert_i64_unsigned_truncate
f64_promote_f32_truncate
```

###  build

`nix` the package manager is required:

```
https://nixos.org/download.html (e.g. `Single-user installation`)
```

this repository follows the instructions in https://gitlab.haskell.org/ghc/ghc-wasm-meta:

```
  ./shell-WebAssembly.sh
  ./compile-RoundingFiasco.sh

  echo "output files:"
  ls RoundingFiasco.wasm
```
