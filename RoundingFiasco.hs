{-# LANGUAGE BinaryLiterals #-}
{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE MagicHash #-}
{-# LANGUAGE Rank2Types #-}
{-# LANGUAGE NegativeLiterals #-}
{-# LANGUAGE ExtendedLiterals #-}

module RoundingFiasco() where

import GHC.Exts
import GHC.Prim
import GHC.Stack
import Data.Bits
import GHC.Float
import GHC.Word
import GHC.Int
import Data.Int
import Data.List
import System.Exit

import OperationCode


testcase
  :: Word32
  -> Word64
  -> Word64
  -> Word64
  -> IO ()
testcase
  function_id
  supposed_to_be_output
  argument_one
  maybe_used_argument_two
  = result
  where
    result
      = if was_test_successful
          then putStrLn $ show ("test was successful", function_id, showOpCode function_id, supposed_to_be_output, argument_one, maybe_used_argument_two)
          else do
            print $ ("function id:", function_id, showOpCode function_id)
            print $ ("supposed_to_be_output:", supposed_to_be_output, visualize supposed_to_be_output)
            print $ ("left argument:", argument_one, visualize argument_one)
            print $ ("right argument:", maybe_used_argument_two, visualize maybe_used_argument_two)
            print $ ("actual result:", test_calculation, visualize test_calculation)
            die "unexpected mismatch"

    test_calculation
      = invoke_by_function_id
          function_id
          argument_one
          maybe_used_argument_two

    was_test_successful = supposed_to_be_output == test_calculation

    visualize word64 = (as_f32, as_f64)
      where
        as_f32 = castWord32ToFloat $ fromInteger $ toInteger $ word64
        as_f64 = castWord64ToDouble $ fromInteger $ toInteger $ word64


invoke_by_function_id
  :: Word32
  -> Word64
  -> Word64
  -> Word64
invoke_by_function_id
  function_id
  left
  right
  = result
  where
    as_f32 bits = castWord32ToFloat $ fromInteger $ toInteger $ bits
    as_f64 bits = castWord64ToDouble $ fromInteger $ toInteger $ bits

    left_as_i32_unsigned :: Word32
    left_as_i32_unsigned = fromInteger $ toInteger $ left
    left_as_i64_unsigned :: Word64
    left_as_i64_unsigned = left

    -- TODO why not in standard library?
    word64ToInt64 (W64# x) = I64# (word64ToInt64# x)
    word32ToInt32 (W32# x) = I32# (word32ToInt32# x)

    left_as_i32_signed :: Int32
    left_as_i32_signed = word32ToInt32 $ fromInteger $ toInteger $ left
    left_as_i64_signed :: Int64
    left_as_i64_signed = word64ToInt64 left

    left_as_f32 = as_f32 left
    left_as_f64 = as_f64 left

    right_as_f32 = as_f32 right
    right_as_f64 = as_f64 right

    from_i32_unsigned = fromInteger . toInteger
    from_i32_signed = fromInteger . toInteger
    from_f32 = from_i32_unsigned . castFloatToWord32
    from_f64 = castDoubleToWord64

    result = case function_id of
      (0x1C) -> from_i32_signed $ i32_sign_bit_f32                  left_as_f32
      (0x1D) -> from_i32_signed $ i32_arithmic_signum_f32           left_as_f32
      (0x1E) -> from_i32_signed $ i32_sign_bit_f64                  left_as_f64
      (0x1F) -> from_i32_signed $ i32_arithmic_signum_f64           left_as_f64
      (0x20) -> from_f32        $ f32_squareRoot_ceil               left_as_f32
      (0x21) -> from_f32        $ f32_add_ceil                      left_as_f32 right_as_f32
      (0x22) -> from_f32        $ f32_subtract_ceil                 left_as_f32 right_as_f32
      (0x23) -> from_f32        $ f32_multiplicate_ceil             left_as_f32 right_as_f32
      (0x24) -> from_f32        $ f32_divide_ceil                   left_as_f32 right_as_f32
      (0x25) -> from_f64        $ f64_squareRoot_ceil               left_as_f64
      (0x26) -> from_f64        $ f64_add_ceil                      left_as_f64 right_as_f64
      (0x27) -> from_f64        $ f64_subtract_ceil                 left_as_f64 right_as_f64
      (0x28) -> from_f64        $ f64_multiplicate_ceil             left_as_f64 right_as_f64
      (0x29) -> from_f64        $ f64_divide_ceil                   left_as_f64 right_as_f64
      (0x32) -> from_f32        $ f32_convert_i32_signed_ceil       left_as_i32_signed
      (0x33) -> from_f32        $ f32_convert_i32_unsigned_ceil     left_as_i32_unsigned
      (0x34) -> from_f32        $ f32_convert_i64_signed_ceil       left_as_i64_signed
      (0x35) -> from_f32        $ f32_convert_i64_unsigned_ceil     left_as_i64_unsigned
      (0x36) -> from_f32        $ f32_demote_f64_ceil               left_as_f64
      (0x37) -> from_f64        $ f64_convert_i32_signed_ceil       left_as_i32_signed
      (0x38) -> from_f64        $ f64_convert_i32_unsigned_ceil     left_as_i32_unsigned
      (0x39) -> from_f64        $ f64_convert_i64_signed_ceil       left_as_i64_signed
      (0x3A) -> from_f64        $ f64_convert_i64_unsigned_ceil     left_as_i64_unsigned
      (0x3B) -> from_f64        $ f64_promote_f32_ceil              left_as_f32
      (0x40) -> from_f32        $ f32_squareRoot_floor              left_as_f32
      (0x41) -> from_f32        $ f32_add_floor                     left_as_f32 right_as_f32
      (0x42) -> from_f32        $ f32_subtract_floor                left_as_f32 right_as_f32
      (0x43) -> from_f32        $ f32_multiplicate_floor            left_as_f32 right_as_f32
      (0x44) -> from_f32        $ f32_divide_floor                  left_as_f32 right_as_f32
      (0x45) -> from_f64        $ f64_squareRoot_floor              left_as_f64
      (0x46) -> from_f64        $ f64_add_floor                     left_as_f64 right_as_f64
      (0x47) -> from_f64        $ f64_subtract_floor                left_as_f64 right_as_f64
      (0x48) -> from_f64        $ f64_multiplicate_floor            left_as_f64 right_as_f64
      (0x49) -> from_f64        $ f64_divide_floor                  left_as_f64 right_as_f64
      (0x52) -> from_f32        $ f32_convert_i32_signed_floor      left_as_i32_signed
      (0x53) -> from_f32        $ f32_convert_i32_unsigned_floor    left_as_i32_unsigned
      (0x54) -> from_f32        $ f32_convert_i64_signed_floor      left_as_i64_signed
      (0x55) -> from_f32        $ f32_convert_i64_unsigned_floor    left_as_i64_unsigned
      (0x56) -> from_f32        $ f32_demote_f64_floor              left_as_f64
      (0x57) -> from_f64        $ f64_convert_i32_signed_floor      left_as_i32_signed
      (0x58) -> from_f64        $ f64_convert_i32_unsigned_floor    left_as_i32_unsigned
      (0x59) -> from_f64        $ f64_convert_i64_signed_floor      left_as_i64_signed
      (0x5A) -> from_f64        $ f64_convert_i64_unsigned_floor    left_as_i64_unsigned
      (0x5B) -> from_f64        $ f64_promote_f32_floor             left_as_f32
      (0x60) -> from_f32        $ f32_squareRoot_truncate           left_as_f32
      (0x61) -> from_f32        $ f32_add_truncate                  left_as_f32 right_as_f32
      (0x62) -> from_f32        $ f32_subtract_truncate             left_as_f32 right_as_f32
      (0x63) -> from_f32        $ f32_multiplicate_truncate         left_as_f32 right_as_f32
      (0x64) -> from_f32        $ f32_divide_truncate               left_as_f32 right_as_f32
      (0x65) -> from_f64        $ f64_squareRoot_truncate           left_as_f64
      (0x66) -> from_f64        $ f64_add_truncate                  left_as_f64 right_as_f64
      (0x67) -> from_f64        $ f64_subtract_truncate             left_as_f64 right_as_f64
      (0x68) -> from_f64        $ f64_multiplicate_truncate         left_as_f64 right_as_f64
      (0x69) -> from_f64        $ f64_divide_truncate               left_as_f64 right_as_f64
      (0x72) -> from_f32        $ f32_convert_i32_signed_truncate   left_as_i32_signed
      (0x73) -> from_f32        $ f32_convert_i32_unsigned_truncate left_as_i32_unsigned
      (0x74) -> from_f32        $ f32_convert_i64_signed_truncate   left_as_i64_signed
      (0x75) -> from_f32        $ f32_convert_i64_unsigned_truncate left_as_i64_unsigned
      (0x76) -> from_f32        $ f32_demote_f64_truncate           left_as_f64
      (0x77) -> from_f64        $ f64_convert_i32_signed_truncate   left_as_i32_signed
      (0x78) -> from_f64        $ f64_convert_i32_unsigned_truncate left_as_i32_unsigned
      (0x79) -> from_f64        $ f64_convert_i64_signed_truncate   left_as_i64_signed
      (0x7A) -> from_f64        $ f64_convert_i64_unsigned_truncate left_as_i64_unsigned
      (0x7B) -> from_f64        $ f64_promote_f32_truncate          left_as_f32








{-



|prefix|opcode|opcode binary|name                 | pretty string      |
|------|-------|--------------|----------------------|---------------------|
| 0xFC | 0x1C | 0b00011100 | f32.binary_sign         | binary_sign         |
| 0xFC | 0x1D | 0b00011101 | i32.trinary_sign_f32    | trinary_sign_f32    |
| 0xFC | 0x1E | 0b00011110 | f64.binary_sign         | binary_sign         |
| 0xFC | 0x1F | 0b00011111 | i32.trinary_sign_f64    | trinary_sign_f64    |
| 0xFC | 0x20 | 0b00100000 | f32.sqrt_ceil           | sqrt_ceil           |
| 0xFC | 0x21 | 0b00100001 | f32.add_ceil            | +_ceil              |
| 0xFC | 0x22 | 0b00100010 | f32.sub_ceil            | -_ceil              |
| 0xFC | 0x23 | 0b00100011 | f32.mul_ceil            | *_ceil              |
| 0xFC | 0x24 | 0b00100100 | f32.div_ceil            | /_ceil              |
| 0xFC | 0x25 | 0b00100101 | f64.sqrt_ceil           | sqrt_ceil           |
| 0xFC | 0x26 | 0b00100110 | f64.add_ceil            | +_ceil              |
| 0xFC | 0x27 | 0b00100111 | f64.sub_ceil            | -_ceil              |
| 0xFC | 0x28 | 0b00101000 | f64.mul_ceil            | *_ceil              |
| 0xFC | 0x29 | 0b00101001 | f64.div_ceil            | /_ceil              |
| 0xFC | 0x32 | 0b00110010 | f32.convert_i32_s_ceil  | convert_i32_s_ceil  |
| 0xFC | 0x33 | 0b00110011 | f32.convert_i32_u_ceil  | convert_i32_u_ceil  |
| 0xFC | 0x34 | 0b00110100 | f32.convert_i64_s_ceil  | convert_i64_s_ceil  |
| 0xFC | 0x35 | 0b00110101 | f32.convert_i64_u_ceil  | convert_i64_u_ceil  |
| 0xFC | 0x36 | 0b00110110 | f32.demote_f64_ceil     | demote_f64_ceil     |
| 0xFC | 0x37 | 0b00110111 | f64.convert_i32_s_ceil  | convert_i32_s_ceil  |
| 0xFC | 0x38 | 0b00111000 | f64.convert_i32_u_ceil  | convert_i32_u_ceil  |
| 0xFC | 0x39 | 0b00111001 | f64.convert_i64_s_ceil  | convert_i64_s_ceil  |
| 0xFC | 0x3A | 0b00111010 | f64.convert_i64_u_ceil  | convert_i64_u_ceil  |
| 0xFC | 0x3B | 0b00111011 | f64.promote_f32_ceil    | promote_f32_ceil    |
| 0xFC | 0x40 | 0b01000000 | f32.sqrt_floor          | sqrt_floor          |
| 0xFC | 0x41 | 0b01000001 | f32.add_floor           | +_floor             |
| 0xFC | 0x42 | 0b01000010 | f32.sub_floor           | -_floor             |
| 0xFC | 0x43 | 0b01000011 | f32.mul_floor           | *_floor             |
| 0xFC | 0x44 | 0b01000100 | f32.div_floor           | /_floor             |
| 0xFC | 0x45 | 0b01000101 | f64.sqrt_floor          | sqrt_floor          |
| 0xFC | 0x46 | 0b01000110 | f64.add_floor           | +_floor             |
| 0xFC | 0x47 | 0b01000111 | f64.sub_floor           | -_floor             |
| 0xFC | 0x48 | 0b01001000 | f64.mul_floor           | *_floor             |
| 0xFC | 0x49 | 0b01001001 | f64.div_floor           | /_floor             |
| 0xFC | 0x52 | 0b01010010 | f32.convert_i32_s_floor | convert_i32_s_floor |
| 0xFC | 0x53 | 0b01010011 | f32.convert_i32_u_floor | convert_i32_u_floor |
| 0xFC | 0x54 | 0b01010100 | f32.convert_i64_s_floor | convert_i64_s_floor |
| 0xFC | 0x55 | 0b01010101 | f32.convert_i64_u_floor | convert_i64_u_floor |
| 0xFC | 0x56 | 0b01010110 | f32.demote_f64_floor    | demote_f64_floor    |
| 0xFC | 0x57 | 0b01010111 | f64.convert_i32_s_floor | convert_i32_s_floor |
| 0xFC | 0x58 | 0b01011000 | f64.convert_i32_u_floor | convert_i32_u_floor |
| 0xFC | 0x59 | 0b01011001 | f64.convert_i64_s_floor | convert_i64_s_floor |
| 0xFC | 0x5A | 0b01011010 | f64.convert_i64_u_floor | convert_i64_u_floor |
| 0xFC | 0x5B | 0b01011011 | f64.promote_f32_floor   | promote_f32_floor   |
| 0xFC | 0x60 | 0b01100000 | f32.sqrt_trunc          | sqrt_trunc          |
| 0xFC | 0x61 | 0b01100001 | f32.add_trunc           | +_trunc             |
| 0xFC | 0x62 | 0b01100010 | f32.sub_trunc           | -_trunc             |
| 0xFC | 0x63 | 0b01100011 | f32.mul_trunc           | *_trunc             |
| 0xFC | 0x64 | 0b01100100 | f32.div_trunc           | /_trunc             |
| 0xFC | 0x65 | 0b01100101 | f64.sqrt_trunc          | sqrt_trunc          |
| 0xFC | 0x66 | 0b01100110 | f64.add_trunc           | +_trunc             |
| 0xFC | 0x67 | 0b01100111 | f64.sub_trunc           | -_trunc             |
| 0xFC | 0x68 | 0b01101000 | f64.mul_trunc           | *_trunc             |
| 0xFC | 0x69 | 0b01101001 | f64.div_trunc           | /_trunc             |
| 0xFC | 0x72 | 0b01110010 | f32.convert_i32_s_trunc | convert_i32_s_trunc |
| 0xFC | 0x73 | 0b01110011 | f32.convert_i32_u_trunc | convert_i32_u_trunc |
| 0xFC | 0x74 | 0b01110100 | f32.convert_i64_s_trunc | convert_i64_s_trunc |
| 0xFC | 0x75 | 0b01110101 | f32.convert_i64_u_trunc | convert_i64_u_trunc |
| 0xFC | 0x76 | 0b01110110 | f32.demote_f64_trunc    | demote_f64_trunc    |
| 0xFC | 0x77 | 0b01110111 | f64.convert_i32_s_trunc | convert_i32_s_trunc |
| 0xFC | 0x78 | 0b01111000 | f64.convert_i32_u_trunc | convert_i32_u_trunc |
| 0xFC | 0x79 | 0b01111001 | f64.convert_i64_s_trunc | convert_i64_s_trunc |
| 0xFC | 0x7A | 0b01111010 | f64.convert_i64_u_trunc | convert_i64_u_trunc |
| 0xFC | 0x7B | 0b01111011 | f64.promote_f32_trunc   | promote_f32_trunc   |



-}





------------
------------

-- foreign export ccall debug :: IO ()
foreign export ccall outputInterfaceVariations :: IO ()
-- foreign export ccall testsuite :: Bool


foreign export ccall testcase
  :: Word32
  -> Word64
  -> Word64
  -> Word64
  -> IO ()



-- manual:

foreign export ccall f32_demote_f64_ceil :: Double -> Float
f32_demote_f64_ceil :: Double -> Float
f32_demote_f64_ceil = sanatizeNaNDemotion $ sanatizeNegtiveZeroDemotionPromotion $ convert RoundUp

foreign export ccall f32_demote_f64_floor :: Double -> Float
f32_demote_f64_floor :: Double -> Float
f32_demote_f64_floor = sanatizeNaNDemotion $ sanatizeNegtiveZeroDemotionPromotion $ convert RoundDown

foreign export ccall f32_demote_f64_truncate :: Double -> Float
f32_demote_f64_truncate :: Double -> Float
f32_demote_f64_truncate = sanatizeNaNDemotion $ sanatizeNegtiveZeroDemotionPromotion $ convert Truncate



foreign export ccall f64_promote_f32_ceil :: Float -> Double
f64_promote_f32_ceil :: Float -> Double
f64_promote_f32_ceil = sanatizeNaNPromotion $ sanatizeNegtiveZeroDemotionPromotion $ convert RoundUp

foreign export ccall f64_promote_f32_floor :: Float -> Double
f64_promote_f32_floor :: Float -> Double
f64_promote_f32_floor = sanatizeNaNPromotion $ sanatizeNegtiveZeroDemotionPromotion $ convert RoundDown

foreign export ccall f64_promote_f32_truncate :: Float -> Double
f64_promote_f32_truncate :: Float -> Double
f64_promote_f32_truncate = sanatizeNaNPromotion $ sanatizeNegtiveZeroDemotionPromotion $ convert Truncate


foreign export ccall i32_arithmic_signum_f32 :: Float -> Int32
foreign export ccall i32_arithmic_signum_f64 :: Double -> Int32
foreign export ccall i32_sign_bit_f32 :: Float -> Int32
foreign export ccall i32_sign_bit_f64 :: Double -> Int32


-- generated

{-begin auto generated-}
foreign export ccall f32_squareRoot_ceil :: Float -> Float
f32_squareRoot_ceil :: Float -> Float
f32_squareRoot_ceil = squareRoot RoundUp

foreign export ccall f32_squareRoot_floor :: Float -> Float
f32_squareRoot_floor :: Float -> Float
f32_squareRoot_floor = squareRoot RoundDown

foreign export ccall f32_squareRoot_truncate :: Float -> Float
f32_squareRoot_truncate :: Float -> Float
f32_squareRoot_truncate = squareRoot Truncate

foreign export ccall f64_squareRoot_ceil :: Double -> Double
f64_squareRoot_ceil :: Double -> Double
f64_squareRoot_ceil = squareRoot RoundUp

foreign export ccall f64_squareRoot_floor :: Double -> Double
f64_squareRoot_floor :: Double -> Double
f64_squareRoot_floor = squareRoot RoundDown

foreign export ccall f64_squareRoot_truncate :: Double -> Double
f64_squareRoot_truncate :: Double -> Double
f64_squareRoot_truncate = squareRoot Truncate

foreign export ccall f32_add_ceil :: Float -> Float -> Float
f32_add_ceil :: Float -> Float -> Float
f32_add_ceil = binary_operator_rounded RoundUp (+)

foreign export ccall f32_add_floor :: Float -> Float -> Float
f32_add_floor :: Float -> Float -> Float
f32_add_floor = sanitizeZeroForAddition $ binary_operator_rounded RoundDown (+)

foreign export ccall f32_add_truncate :: Float -> Float -> Float
f32_add_truncate :: Float -> Float -> Float
f32_add_truncate = binary_operator_rounded Truncate (+)

foreign export ccall f32_subtract_ceil :: Float -> Float -> Float
f32_subtract_ceil :: Float -> Float -> Float
f32_subtract_ceil = binary_operator_rounded RoundUp (-)

foreign export ccall f32_subtract_floor :: Float -> Float -> Float
f32_subtract_floor :: Float -> Float -> Float
f32_subtract_floor = sanitizeZeroForSubtraction $ binary_operator_rounded RoundDown (-)

foreign export ccall f32_subtract_truncate :: Float -> Float -> Float
f32_subtract_truncate :: Float -> Float -> Float
f32_subtract_truncate = binary_operator_rounded Truncate (-)

foreign export ccall f32_multiplicate_ceil :: Float -> Float -> Float
f32_multiplicate_ceil :: Float -> Float -> Float
f32_multiplicate_ceil = binary_operator_rounded RoundUp (*)

foreign export ccall f32_multiplicate_floor :: Float -> Float -> Float
f32_multiplicate_floor :: Float -> Float -> Float
f32_multiplicate_floor = binary_operator_rounded RoundDown (*)

foreign export ccall f32_multiplicate_truncate :: Float -> Float -> Float
f32_multiplicate_truncate :: Float -> Float -> Float
f32_multiplicate_truncate = binary_operator_rounded Truncate (*)

foreign export ccall f32_divide_ceil :: Float -> Float -> Float
f32_divide_ceil :: Float -> Float -> Float
f32_divide_ceil = binary_operator_rounded RoundUp (/)

foreign export ccall f32_divide_floor :: Float -> Float -> Float
f32_divide_floor :: Float -> Float -> Float
f32_divide_floor = binary_operator_rounded RoundDown (/)

foreign export ccall f32_divide_truncate :: Float -> Float -> Float
f32_divide_truncate :: Float -> Float -> Float
f32_divide_truncate = binary_operator_rounded Truncate (/)

foreign export ccall f64_add_ceil :: Double -> Double -> Double
f64_add_ceil :: Double -> Double -> Double
f64_add_ceil = binary_operator_rounded RoundUp (+)

foreign export ccall f64_add_floor :: Double -> Double -> Double
f64_add_floor :: Double -> Double -> Double
f64_add_floor = sanitizeZeroForAddition $ binary_operator_rounded RoundDown (+)

foreign export ccall f64_add_truncate :: Double -> Double -> Double
f64_add_truncate :: Double -> Double -> Double
f64_add_truncate = binary_operator_rounded Truncate (+)

foreign export ccall f64_subtract_ceil :: Double -> Double -> Double
f64_subtract_ceil :: Double -> Double -> Double
f64_subtract_ceil = binary_operator_rounded RoundUp (-)

foreign export ccall f64_subtract_floor :: Double -> Double -> Double
f64_subtract_floor :: Double -> Double -> Double
f64_subtract_floor = sanitizeZeroForSubtraction $ binary_operator_rounded RoundDown (-)

foreign export ccall f64_subtract_truncate :: Double -> Double -> Double
f64_subtract_truncate :: Double -> Double -> Double
f64_subtract_truncate = binary_operator_rounded Truncate (-)

foreign export ccall f64_multiplicate_ceil :: Double -> Double -> Double
f64_multiplicate_ceil :: Double -> Double -> Double
f64_multiplicate_ceil = binary_operator_rounded RoundUp (*)

foreign export ccall f64_multiplicate_floor :: Double -> Double -> Double
f64_multiplicate_floor :: Double -> Double -> Double
f64_multiplicate_floor = binary_operator_rounded RoundDown (*)

foreign export ccall f64_multiplicate_truncate :: Double -> Double -> Double
f64_multiplicate_truncate :: Double -> Double -> Double
f64_multiplicate_truncate = binary_operator_rounded Truncate (*)

foreign export ccall f64_divide_ceil :: Double -> Double -> Double
f64_divide_ceil :: Double -> Double -> Double
f64_divide_ceil = binary_operator_rounded RoundUp (/)

foreign export ccall f64_divide_floor :: Double -> Double -> Double
f64_divide_floor :: Double -> Double -> Double
f64_divide_floor = binary_operator_rounded RoundDown (/)

foreign export ccall f64_divide_truncate :: Double -> Double -> Double
f64_divide_truncate :: Double -> Double -> Double
f64_divide_truncate = binary_operator_rounded Truncate (/)

foreign export ccall f32_convert_i32_unsigned_ceil :: Word32 -> Float
f32_convert_i32_unsigned_ceil :: Word32 -> Float
f32_convert_i32_unsigned_ceil = convert RoundUp

foreign export ccall f32_convert_i32_signed_ceil :: Int32 -> Float
f32_convert_i32_signed_ceil :: Int32 -> Float
f32_convert_i32_signed_ceil = convert RoundUp

foreign export ccall f32_convert_i32_unsigned_floor :: Word32 -> Float
f32_convert_i32_unsigned_floor :: Word32 -> Float
f32_convert_i32_unsigned_floor = convert RoundDown

foreign export ccall f32_convert_i32_signed_floor :: Int32 -> Float
f32_convert_i32_signed_floor :: Int32 -> Float
f32_convert_i32_signed_floor = convert RoundDown

foreign export ccall f32_convert_i32_unsigned_truncate :: Word32 -> Float
f32_convert_i32_unsigned_truncate :: Word32 -> Float
f32_convert_i32_unsigned_truncate = convert Truncate

foreign export ccall f32_convert_i32_signed_truncate :: Int32 -> Float
f32_convert_i32_signed_truncate :: Int32 -> Float
f32_convert_i32_signed_truncate = convert Truncate

foreign export ccall f32_convert_i64_unsigned_ceil :: Word64 -> Float
f32_convert_i64_unsigned_ceil :: Word64 -> Float
f32_convert_i64_unsigned_ceil = convert RoundUp

foreign export ccall f32_convert_i64_signed_ceil :: Int64 -> Float
f32_convert_i64_signed_ceil :: Int64 -> Float
f32_convert_i64_signed_ceil = convert RoundUp

foreign export ccall f32_convert_i64_unsigned_floor :: Word64 -> Float
f32_convert_i64_unsigned_floor :: Word64 -> Float
f32_convert_i64_unsigned_floor = convert RoundDown

foreign export ccall f32_convert_i64_signed_floor :: Int64 -> Float
f32_convert_i64_signed_floor :: Int64 -> Float
f32_convert_i64_signed_floor = convert RoundDown

foreign export ccall f32_convert_i64_unsigned_truncate :: Word64 -> Float
f32_convert_i64_unsigned_truncate :: Word64 -> Float
f32_convert_i64_unsigned_truncate = convert Truncate

foreign export ccall f32_convert_i64_signed_truncate :: Int64 -> Float
f32_convert_i64_signed_truncate :: Int64 -> Float
f32_convert_i64_signed_truncate = convert Truncate

foreign export ccall f64_convert_i32_unsigned_ceil :: Word32 -> Double
f64_convert_i32_unsigned_ceil :: Word32 -> Double
f64_convert_i32_unsigned_ceil = convert RoundUp

foreign export ccall f64_convert_i32_signed_ceil :: Int32 -> Double
f64_convert_i32_signed_ceil :: Int32 -> Double
f64_convert_i32_signed_ceil = convert RoundUp

foreign export ccall f64_convert_i32_unsigned_floor :: Word32 -> Double
f64_convert_i32_unsigned_floor :: Word32 -> Double
f64_convert_i32_unsigned_floor = convert RoundDown

foreign export ccall f64_convert_i32_signed_floor :: Int32 -> Double
f64_convert_i32_signed_floor :: Int32 -> Double
f64_convert_i32_signed_floor = convert RoundDown

foreign export ccall f64_convert_i32_unsigned_truncate :: Word32 -> Double
f64_convert_i32_unsigned_truncate :: Word32 -> Double
f64_convert_i32_unsigned_truncate = convert Truncate

foreign export ccall f64_convert_i32_signed_truncate :: Int32 -> Double
f64_convert_i32_signed_truncate :: Int32 -> Double
f64_convert_i32_signed_truncate = convert Truncate

foreign export ccall f64_convert_i64_unsigned_ceil :: Word64 -> Double
f64_convert_i64_unsigned_ceil :: Word64 -> Double
f64_convert_i64_unsigned_ceil = convert RoundUp

foreign export ccall f64_convert_i64_signed_ceil :: Int64 -> Double
f64_convert_i64_signed_ceil :: Int64 -> Double
f64_convert_i64_signed_ceil = convert RoundUp

foreign export ccall f64_convert_i64_unsigned_floor :: Word64 -> Double
f64_convert_i64_unsigned_floor :: Word64 -> Double
f64_convert_i64_unsigned_floor = convert RoundDown

foreign export ccall f64_convert_i64_signed_floor :: Int64 -> Double
f64_convert_i64_signed_floor :: Int64 -> Double
f64_convert_i64_signed_floor = convert RoundDown

foreign export ccall f64_convert_i64_unsigned_truncate :: Word64 -> Double
f64_convert_i64_unsigned_truncate :: Word64 -> Double
f64_convert_i64_unsigned_truncate = convert Truncate

foreign export ccall f64_convert_i64_signed_truncate :: Int64 -> Double
f64_convert_i64_signed_truncate :: Int64 -> Double
f64_convert_i64_signed_truncate = convert Truncate
{-end auto generated-}




---

-- https://stackoverflow.com/questions/28949774/what-is-0-0-by-ieee-floating-point-standard
sanitizeZeroForAddition
  operator
  a
  b
  = if (0 == x) && (sign_bit a * sign_bit b == -1)
      then negate x
      else x
  where
    x = operator a b

    sign_bit y
      = if 0 == y
          then (boolToNumber $ isNegativeZero y)
          else signum y

    boolToNumber True = -1
    boolToNumber False = 1
sanitizeZeroForSubtraction
  operator
  a
  b
  = if (0 == x) && (sign_bit a == sign_bit b)
      then negate x
      else x
  where
    x = operator a b

    sign_bit y
      = if 0 == y
          then (boolToNumber $ isNegativeZero y)
          else signum y

    boolToNumber True = -1
    boolToNumber False = 1


sanatizeNegtiveZeroDemotionPromotion
  conversion
  input
  = result
  where
    result
      = if isNegativeZero input
          then negate 0.0
          else conversion input


sanatizeNaNDemotion
  conversion
  input
  = result
  where
    result
      = if isNaN input
          then double2Float input
          else conversion input

sanatizeNaNPromotion
  conversion
  input
  = result
  where
    result
      = if isNaN input
          then float2Double input
          else conversion input






---




data HaskellExports = HaskellExports [String]
data GHC_exports = GHC_exports [String]
data ExportStatements
  = ExportStatements
      HaskellExports
      GHC_exports


combine :: ExportStatements -> ExportStatements -> ExportStatements
combine
  (ExportStatements
    (HaskellExports a)
    (GHC_exports b)
  )
  (ExportStatements
    (HaskellExports aa)
    (GHC_exports bb)
  )
  = ExportStatements
      (HaskellExports (a++[""]++aa))
      (GHC_exports (b++bb))


generateVariations :: ExportStatements
generateVariations = result
  where
    result
      = foldr1 combine
      $ []
      ++ unary_operations
      ++ binary_operations
      ++ convert_operations
      ++ manual_ones


    namual_comandline_exports =
      [ "i32_arithmic_signum_f64"
      , "i32_arithmic_signum_f32"
      , "i32_sign_bit_f64"
      , "i32_sign_bit_f32"
      , "f32_demote_f64_ceil"
      , "f32_demote_f64_floor"
      , "f32_demote_f64_truncate"
      , "f64_promote_f32_ceil"
      , "f64_promote_f32_floor"
      , "f64_promote_f32_truncate"
      ]
    manual_ones = [ExportStatements (HaskellExports []) (GHC_exports namual_comandline_exports)]



    f_types = ["f32", "f64"]
    i_types = ["i32", "i64"]
    unary_operation = ["squareRoot"]
    binary_operation = ["add", "subtract", "multiplicate", "divide"]
    rounding_mode = ["ceil", "floor", "truncate"]
    signednesses = ["unsigned", "signed"]

    asHaskellType "f32" = "Float"
    asHaskellType "f64" = "Double"

    asHaskellType_OrWithSigness "unsigned" "i32" = "Word32"
    asHaskellType_OrWithSigness "signed" "i32" = "Int32"
    asHaskellType_OrWithSigness "unsigned" "i64" = "Word64"
    asHaskellType_OrWithSigness "signed" "i64" = "Int64"
    asHaskellType_OrWithSigness x y = error $ show (x,y)

    translateRounding "ceil" = show RoundUp
    translateRounding "floor" = show RoundDown
    translateRounding "truncate" = show Truncate

    translateOperation "add" = "(+)"
    translateOperation "subtract" = "(-)"
    translateOperation "multiplicate" = "(*)"
    translateOperation "divide" = "(/)"

    binary_operations = do
      resultType <- f_types
      operator <- binary_operation
      roundingVariation <- rounding_mode

      let export_name = intercalate "_" [resultType, operator, roundingVariation]
      let export_type
            = let haskell_type = asHaskellType resultType
              in haskell_type ++ " -> " ++ haskell_type ++ " -> " ++ haskell_type

      let export_line = "foreign export ccall "++export_name++" :: " ++ export_type
      let typehelperline = ""++export_name++" :: "++export_type++""
      let specialization
            = let rounding = translateRounding roundingVariation
              in let operatorinparenthesis = translateOperation operator
              in ""++export_name++" = binary_operator_rounded "++rounding++" "++operatorinparenthesis++""

      return
        $ ExportStatements
            (HaskellExports [export_line, typehelperline, specialization])
            (GHC_exports [export_name])

    unary_operations = do
      resultType <- f_types
      operator <- unary_operation
      roundingVariation <- rounding_mode
      let export_name = intercalate "_" [resultType, operator, roundingVariation]
      let export_type
            = let haskell_type = asHaskellType resultType
              in haskell_type ++ " -> " ++ haskell_type
      let export_line = "foreign export ccall "++export_name++" :: " ++ export_type
      let typehelperline = ""++export_name++" :: "++export_type++""
      let specialization
            = let rounding = translateRounding roundingVariation
              in let operatorinparenthesis = translateOperation operator
              in ""++export_name++" = "++operator++" "++rounding
      return
        $ ExportStatements
            (HaskellExports [export_line, typehelperline, specialization])
            (GHC_exports [export_name])


    convert_operations = do
      resultType <- f_types
      inputType <- i_types
      roundingVariation <- rounding_mode
      signedness <- signednesses
      let export_name = intercalate "_" [resultType, "convert", inputType, signedness, roundingVariation]
      let export_type
            = let asHaskell = asHaskellType_OrWithSigness signedness
              in (asHaskell inputType) ++ " -> " ++ (asHaskellType resultType)
      let export_line = "foreign export ccall "++export_name++" :: " ++ export_type
      let typehelperline = ""++export_name++" :: "++export_type++""
      let specialization
            = let rounding = translateRounding roundingVariation
              in ""++export_name++" = "++"convert"++" "++rounding
      return
        $ ExportStatements
            (HaskellExports [export_line, typehelperline, specialization])
            (GHC_exports [export_name])








outputInterfaceVariations
  = do
  putStrLn ""
  mapM_ putStrLn haskell_lines

  putStrLn ""
  putStrLn ""

  putStrLn $ comandlineParameter >>= (\x -> ",--export=" ++ x)

  error "stop"
  where
    ExportStatements
      (HaskellExports haskell_lines)
      (GHC_exports comandlineParameter)
      = generateVariations





------------
------------






debug :: IO ()
debug
  = error
  $ ("debug_output: " ++)
  $ show

  $ f32_divide_floor 1 (1.0e-45)



------------
------------

check
  :: String
  -> Bool
  -> Bool
check note True = True
check note False = error $ note

testsuite = True
  {-
  && check "testing the testsuite" False
  -}
  && check "test_squareRootRoundingDown" test_squareRootRoundingDown
  && check "test_squareRootRoundingUp" test_squareRootRoundingUp
  && check "arithmic_signum" test_arithmic_signum
  && check "test_symatric_sign_bit" test_symatric_sign_bit
  && check "test_nudging_from_infinity" test_nudging_from_infinity



test_squareRootRoundingDown
  = result
  where
    result
      = standardRoundingIsOff_for_n
      && butNewImplementationIsNot

    n = 2

    standardRoundingIsOff_for_n
      = not
      $ is_squareRootImplementationRoundingDown sqrt
      $ n

    butNewImplementationIsNot
      = is_squareRootImplementationRoundingDown (squareRoot RoundDown)
      $ n

    is_squareRootImplementationRoundingDown
      sqrt_implementation
      n
      = ((\x->x*x) (toRational (sqrt_implementation (castWord64ToDouble n))))
      <= toRational (castWord64ToDouble n)



test_squareRootRoundingUp
  = result
  where
    result
      = standardRoundingIsOff_for_n
      && butNewImplementationIsNot

    n = 3

    standardRoundingIsOff_for_n
      = not
      $ is_squareRootImplementationRoundingUp sqrt
      $ n

    butNewImplementationIsNot
      = is_squareRootImplementationRoundingUp (squareRoot RoundUp)
      $ n

    is_squareRootImplementationRoundingUp
      sqrt_implementation
      n
      = preimage
      <= actuallyUsedPreimage
      where
        preimage = toRational (castWord64ToDouble n)

        actuallyUsedPreimage
          = (\x->x*x)
          $ toRational
          $ sqrt_implementation
          $ castWord64ToDouble
          $ n



test_arithmic_signum
  = True
  && (i32_arithmic_signum_f64 (-0.0) == 0)
  && (i32_arithmic_signum_f64 (0.0) == 0)
  && (i32_arithmic_signum_f64 (-1.0) == (-1))
  && (i32_arithmic_signum_f64 (1.0) == (1))
  && (i32_arithmic_signum_f64 (1/0) == (1))
  && (i32_arithmic_signum_f64 (-1/0) == (-1))
  && (i32_arithmic_signum_f64 (0/0) == 0) -- happens to 0 by the used formula


sign_bit_of_negative_numbers = 1
sign_bit_of_positiv_numbers = 0

test_symatric_sign_bit
  = True
  && (i32_sign_bit_f64 (-100) == i32_arithmic_signum_f64 (100))
  && (i32_sign_bit_f64 (0.0) == i32_arithmic_signum_f64 (0.0))
  && (i32_sign_bit_f64 (0.0) == i32_arithmic_signum_f64 (-0.0))
  && (i32_sign_bit_f64 (0.0) == sign_bit_of_positiv_numbers)
  && (i32_sign_bit_f64 (-0.0) == sign_bit_of_negative_numbers)
  && (i32_sign_bit_f64 (- 1/0) == sign_bit_of_negative_numbers)
  && (i32_sign_bit_f64 (1/0) == sign_bit_of_positiv_numbers)
  && (i32_sign_bit_f64 ( 0/0) == sign_bit_of_negative_numbers)
  && (i32_sign_bit_f64 (- 0/0) == sign_bit_of_positiv_numbers)


test_nudging_from_infinity
  = True
  && (f32_multiplicate_floor (-3.4028235e38) (-3.4028235e38) == (3.4028235e38))
  && (isInfinite $ f32_multiplicate_ceil (-3.4028235e38) (-3.4028235e38))
  && (f32_divide_floor 1 1.0e-45 == 3.4028235e38)
  && (isInfinite $ f32_divide_ceil 1 1.0e-45)




------------
------------




assertNote :: String -> Bool -> a -> a
-- assertNote note True value = value
-- assertNote note False value = error note
assertNote _ _ value = value


------------
------------


data RationalWithSpecials
  = NegativInfinity
  | Exisiting Rational
  | PositiveInfinity
  deriving (Show, Eq)


getExisiting _ (Exisiting x) = x
getExisiting defaultValue _ = defaultValue


-- | left: exact result, right: wannebe result
liftComparator _ sorted (Exisiting x) (Exisiting y) = x `sorted` y

liftComparator RoundDown _ PositiveInfinity PositiveInfinity = True
liftComparator RoundDown _ NegativInfinity NegativInfinity = error "?2"

liftComparator RoundDown _ _ PositiveInfinity = False
liftComparator RoundDown _ _ NegativInfinity = True

liftComparator RoundUp _ PositiveInfinity PositiveInfinity = True
liftComparator RoundUp _ NegativInfinity NegativInfinity = error "?4"
liftComparator RoundUp _ _ PositiveInfinity = True
liftComparator RoundUp _ _ NegativInfinity = False

liftComparator Truncate _ xx@(Exisiting x) PositiveInfinity = if 0 < x then liftComparator RoundDown undefined xx PositiveInfinity else error "off nye sign"
liftComparator Truncate _ xx@(Exisiting x) NegativInfinity = if x < 0 then liftComparator RoundUp undefined xx NegativInfinity else error "off nye sign"

liftComparator Truncate _ PositiveInfinity PositiveInfinity = True

liftComparator roundDirection sorted (x) (y) = error $ show ("liftComparator", roundDirection, x, y)


liftComparatorConvert _ sorted (Exisiting x) (Exisiting y) = x `sorted` y
liftComparatorConvert RoundDown sorted PositiveInfinity (Exisiting _) = False
liftComparatorConvert RoundDown _ NegativInfinity (Exisiting _) = True

liftComparatorConvert RoundUp sorted PositiveInfinity (Exisiting _) = True
liftComparatorConvert RoundUp sorted NegativInfinity (Exisiting _) = False

liftComparatorConvert Truncate _ PositiveInfinity xx@(Exisiting x) = if 0 < x then liftComparatorConvert RoundDown undefined PositiveInfinity xx else error "off nye sign"
liftComparatorConvert Truncate _ NegativInfinity xx@(Exisiting x) = if x < 0 then liftComparatorConvert RoundUp undefined NegativInfinity xx else error "off nye sign"

liftComparatorConvert _ sorted PositiveInfinity PositiveInfinity = True
liftComparatorConvert _ sorted NegativInfinity NegativInfinity = True

liftComparatorConvert roundDirection sorted (x) (y) = error $ show ("liftComparatorConvert", roundDirection, x, y)

liftUnary f (Exisiting x) = Exisiting $ f x
liftUnary _ PositiveInfinity = PositiveInfinity
liftUnary _ NegativInfinity = PositiveInfinity


class RationalWithSpecialsLike a where
  exactToRational :: HasCallStack => a -> RationalWithSpecials


class
  (Show a, Num a, RealFloat a, RationalWithSpecialsLike a)
  => IEEE_RoundingFuckUp_Correctable a
  where
  exactFromRational :: HasCallStack => RationalWithSpecials -> a
  nudgeDown :: a -> a
  nudgeUp :: a -> a
  nudgeToZero :: a -> a
  nudgeToZero j
    = if j < 0
        then nudgeUp j
        else if 0 < j
          then nudgeDown j
          else error "what now?"
  nudgeAwayFromZero :: a -> a
  nudgeAwayFromZero j
    = if j < 0
        then nudgeDown j
        else if 0 < j
          then nudgeUp j
          else error "what now?"
  rawToInteger :: a -> Integer
  name :: a -> String


instance RationalWithSpecialsLike Word32 where
  exactToRational = Exisiting . toRational
instance RationalWithSpecialsLike Word64 where
  exactToRational = Exisiting . toRational
instance RationalWithSpecialsLike Int32 where
  exactToRational = Exisiting . toRational
instance RationalWithSpecialsLike Int64 where
  exactToRational = Exisiting . toRational


instance IEEE_RoundingFuckUp_Correctable Double where
  exactFromRational = withFrozenCallStack hopefullyExactFromRational
  nudgeDown = f64_predecessorIEEE
  nudgeUp = f64_successorIEEE
  rawToInteger = toInteger . castDoubleToWord64
  name = const "Double"

instance RationalWithSpecialsLike Double where
  exactToRational = withFrozenCallStack hopeFullyExactToRational


instance IEEE_RoundingFuckUp_Correctable Float where
  exactFromRational = withFrozenCallStack hopefullyExactFromRational
  nudgeDown = f32_predecessorIEEE
  nudgeUp = f32_successorIEEE
  rawToInteger = toInteger . castFloatToWord32
  name = const "Float"


instance RationalWithSpecialsLike Float where
  exactToRational = withFrozenCallStack hopeFullyExactToRational

hopeFullyExactToRational
  :: (HasCallStack, RealFloat a, Show a)
  => a
  -> RationalWithSpecials
hopeFullyExactToRational
  value
  = id
  $ withFrozenCallStack
  $ assertNote note check
  $ result
  where
    result
      = if isInfinite value
          then case signum value of
                  -1 -> NegativInfinity
                  1 -> PositiveInfinity
          else if not $ isNaN value
            then Exisiting $ toRational value
            else error $ "cannot convert to rational: " ++ show value

    note = "this double cannot be represented as Rational: " ++ show value

    check
      = (not $ isNaN $ value)
      && (not $ isNegativeZero $ value)


hopefullyExactFromRational
  :: (HasCallStack, RealFloat a)
  => RationalWithSpecials
  -> a
hopefullyExactFromRational (NegativInfinity) = -1/0
hopefullyExactFromRational (Exisiting rational) = fromRational rational
hopefullyExactFromRational (PositiveInfinity) = 1/0

------------



f64_successorIEEE#
  :: Double#
  -> Double#
f64_successorIEEE#
  value
  = result
  where
    result = resulting_double

    value_as_word64 :: Word64#
    value_as_word64 = stgDoubleToWord64 value

    sign_bit :: Word64#
    sign_bit
      = 0b1000000000000000000000000000000000000000000000000000000000000000#Word64

    exponent_bits :: Word64#
    exponent_bits
      = 0b0111111111110000000000000000000000000000000000000000000000000000#Word64

    _negativ_infinity :: Word64#
    _negativ_infinity
      = 0b1111111111110000000000000000000000000000000000000000000000000000#Word64

    _least_finit_value :: Word64#
    _least_finit_value
      = 0b1111111111101111111111111111111111111111111111111111111111111111#Word64
      -- -1.7976931348623157e308##

    _least_positiv_value :: Word64#
    _least_positiv_value
      = 0b0000000000000000000000000000000000000000000000000000000000000001#Word64
      -- 5.0e-324##

    _negative_zero = sign_bit

    exponent_bits_of_value :: Word64#
    exponent_bits_of_value = and64# exponent_bits value_as_word64

    sign_bit_of_value :: Word64#
    sign_bit_of_value = and64# sign_bit value_as_word64

    resulting_double
      = case exponent_bits_of_value of
          {- not real value or not negativ zero -}
          0b0111111111110000000000000000000000000000000000000000000000000000#Word64 {-exponent_bits-}
            -> case value_as_word64 of
                0b1111111111110000000000000000000000000000000000000000000000000000#Word64 {-negativ_infinity-}
                  -> -1.7976931348623157e308## {-least_finit_value-}
                _
                  -> value --not real values stay as they are (except for negativ_infinity)

          {- real value or negativ zero -}
          _
            -> case sign_bit_of_value of
                {-positive real value-}
                0b0000000000000000000000000000000000000000000000000000000000000000#Word64
                  -> stgWord64ToDouble (plusWord64# 1#Word64 value_as_word64)

                {-negative real value-}
                _
                  -> case value_as_word64 of
                      {-negative_zero-}
                      0b1000000000000000000000000000000000000000000000000000000000000000#Word64 {-negative_zero-}
                        -> 5.0e-324## {-least_positiv_value-}

                      {-proper negativ value-}
                      _
                        -> stgWord64ToDouble (subWord64# value_as_word64 1#Word64)



f64_successorIEEE
  :: Double
  -> Double
f64_successorIEEE
  (D# value#)
  = D# (f64_successorIEEE# value#)




f64_predecessorIEEE#
  :: Double#
  -> Double#
f64_predecessorIEEE#
  value
  = symetric_result
  where
    symetric_result
      = id
      $ negateDouble#
      $ f64_successorIEEE#
      $ negateDouble#
      $ value

    id :: Double# -> Double#
    id x = x

    infixr 0 $
    ($) :: (Double# -> Double#) -> Double# -> Double#
    f $ x = f x


f64_predecessorIEEE
  :: Double
  -> Double
f64_predecessorIEEE
  (D# value#)
  = D# (f64_predecessorIEEE# value#)





------------




f32_successorIEEE#
  :: Float#
  -> Float#
f32_successorIEEE#
  value
  = result
  where
    result = resulting_float

    -- TODO use preprocessor to deduplicate these guys

    value_as_word64 :: Word32# --TODO recent GHC returns Word32#
    value_as_word64 = stgFloatToWord32 value

    sign_bit :: Word32#
    sign_bit
      = 0b10000000000000000000000000000000#Word32

    exponent_bits :: Word32#
    exponent_bits
      = 0b01111111100000000000000000000000#Word32

    _negativ_infinity :: Word32#
    _negativ_infinity
      = 0b11111111100000000000000000000000#Word32

    _least_finit_value :: Word32#
    _least_finit_value
      = 0b11111111011111111111111111111111#Word32
      -- -3.4028235e38#

    _least_positiv_value :: Word32#
    _least_positiv_value
      = 0b00000000000000000000000000000001#Word32
      -- 1.0e-45#

    _negative_zero = sign_bit

    exponent_bits_of_value :: Word32#
    exponent_bits_of_value = andWord32# exponent_bits value_as_word64

    sign_bit_of_value :: Word32#
    sign_bit_of_value = andWord32# sign_bit value_as_word64

    resulting_float
      = case exponent_bits_of_value of
          {- not real value or not negativ zero -}
          0b01111111100000000000000000000000#Word32 {-exponent_bits-}
            -> case value_as_word64 of
                0b11111111100000000000000000000000#Word32 {-negativ_infinity-}
                  -> -3.4028235e38# {-least_finit_value-}
                _
                  -> value --not real values stay as they are (except for negativ_infinity)

          {- real value or negativ zero -}
          _
            -> case sign_bit_of_value of
                {-positive real value-}
                0b00000000000000000000000000000000#Word32
                  -> stgWord32ToFloat (plusWord32# 1#Word32 value_as_word64)

                {-negative real value-}
                _
                  -> case value_as_word64 of
                      {-negative_zero-}
                      0b10000000000000000000000000000000#Word32 {-negative_zero-}
                        -> 1.0e-45# {-least_positiv_value-}

                      {-proper negativ value-}
                      _
                        -> stgWord32ToFloat (minusWord32# value_as_word64 1#Word32)

    minusWord32# :: Word32# -> Word32# -> Word32#
    minusWord32# x y = wordToWord32# (minusWord# (word32ToWord# x) (word32ToWord# y))


f32_successorIEEE
  :: Float
  -> Float
f32_successorIEEE
  (F# value#)
  = F# (f32_successorIEEE# value#)



f32_predecessorIEEE#
  :: Float#
  -> Float#
f32_predecessorIEEE#
  value
  = result
  where
    symetric_result
      = id
      $ negateFloat#
      $ f32_successorIEEE#
      $ negateFloat#
      $ value

    id :: Float# -> Float#
    id x = x

    infixr 0 $
    ($) :: (Float# -> Float#) -> Float# -> Float#
    f $ x = f x

    result
      = if isNegativeZero (F# symetric_result)
          then 0.0#
          else symetric_result

f32_predecessorIEEE
  :: Float
  -> Float
f32_predecessorIEEE
  (F# value#)
  = F# (f32_predecessorIEEE# value#)





------------





data Round = RoundDown | RoundUp | Truncate
  deriving Show


roundingOrder_binaryOperator
  :: Round
  -> (Rational -> Rational -> Bool)
roundingOrder_binaryOperator RoundDown = (>=)
roundingOrder_binaryOperator RoundUp = (<=)
roundingOrder_binaryOperator Truncate = \exact_result allegedly_rounded_result
  -> if exact_result < 0 && allegedly_rounded_result < 0
        then (roundingOrder_binaryOperator RoundUp) exact_result allegedly_rounded_result
        else if 0 < exact_result && 0 < allegedly_rounded_result
                then roundingOrder_binaryOperator RoundDown exact_result allegedly_rounded_result
                else if 0 == exact_result && 0 == allegedly_rounded_result
                  then True
                  else if 0 == allegedly_rounded_result
                    then True -- zero is allways rounded ok
                    else error $ "roundingOrder_binaryOperator: unconsiderd case: " ++ show (exact_result, allegedly_rounded_result)

    -- exact_result `sorted` rounded_result
    -- -2.0 `sorted` -1.0
    -- 1.0 `sorted` 2.0

roundingOrder_UnaryOperator
  :: Round
  -> (Rational -> Rational -> Bool)
roundingOrder_UnaryOperator RoundDown = (<=)
roundingOrder_UnaryOperator RoundUp = (>=)
roundingOrder_UnaryOperator Truncate = \allegedly_rounded_result exact_result
  -> if allegedly_rounded_result < 0 && exact_result < 0
        then (roundingOrder_UnaryOperator RoundUp) allegedly_rounded_result exact_result
        else if 0 < allegedly_rounded_result && 0 < exact_result
                then roundingOrder_UnaryOperator RoundDown allegedly_rounded_result exact_result
                else if 0 == allegedly_rounded_result && 0 == exact_result
                  then True
                  else if 0 == allegedly_rounded_result
                    then True -- zero is allways rounded ok
                    else error $ "roundingOrder_binaryOperator: unconsiderd case: " ++ show (exact_result, allegedly_rounded_result)


chooseNudging RoundDown = nudgeDown
chooseNudging RoundUp = nudgeUp
chooseNudging Truncate = nudgeToZero

chooseTheTheOtherNudging RoundDown = nudgeUp
chooseTheTheOtherNudging RoundUp = nudgeDown
chooseTheTheOtherNudging Truncate = nudgeAwayFromZero




------------




binary_operator_rounded
  :: Round
  -> (forall a. (Num a, Fractional a) => a -> a -> a)
  -> ((IEEE_RoundingFuckUp_Correctable b) => b -> b -> b)
binary_operator_rounded
  upOrDown
  operator
  left
  right
  = id
--   $ error $ show
--   $ ("binary_operator_rounded", isInfinite right )
  $ result
  where
    sorted
      = liftComparator upOrDown
      $ roundingOrder_binaryOperator upOrDown

    nudge = chooseNudging upOrDown

    the_other_nudge = chooseTheTheOtherNudging upOrDown

    badArguments = error $ show ("case not supported", left, right)
    left_rational = getExisiting badArguments $ exactToRational left
    right_rational = getExisiting badArguments $ exactToRational right

    exact_result :: Rational
    exact_result = left_rational `operator` right_rational

    somehow_rounded_result = left `operator` right

    somehow_rounded_result_as_rational
      = exactToRational
      $ somehow_rounded_result

    was_already_rounded_correctly
      = (Exisiting exact_result) `sorted` somehow_rounded_result_as_rational

    correctly_rounded_result
      = if not (isNaN somehow_rounded_result || 0 == right)
          then if was_already_rounded_correctly
            then somehow_rounded_result
            else nudge somehow_rounded_result
          else somehow_rounded_result


    note
      = "new nudging did not work: "
      ++ show
      ( upOrDown
      , left
      , right
      )

    check
      = is_special_case
      ||
      (True
        && (Exisiting exact_result)
            `sorted`
              (exactToRational correctly_rounded_result)
        && (exactToRational $ the_other_nudge correctly_rounded_result)
            `strict_sorted`
              (Exisiting exact_result)
      )

    strict_sorted x y = (x /= y) && (x `sorted` y)


    is_special_case = False
      || isNaN left
      || isNaN right
      || isNegativeZero left
      || isNegativeZero right
      || isInfinite left
      || isInfinite right

    result
      = if not (isNaN left || isNaN right)
          then normal_case
          else somehow_rounded_result

    normal_case
      = if not(isInfinite left || isInfinite right)
          then propernumbercase
          else somehow_rounded_result

    propernumbercase = assertNote note check correctly_rounded_result






------------

squareRoot
  :: (Floating a, IEEE_RoundingFuckUp_Correctable a)
  => Round
  -> a
  -> a
squareRoot
  upOrDown
  preImage
  = result
  where
    sorted
      = liftComparator upOrDown
      $ roundingOrder_UnaryOperator upOrDown
      -- TODO refactor into one function

    nudge = chooseNudging upOrDown

    the_other_nudge = chooseTheTheOtherNudging upOrDown

    result = normal_case

    somehowRoundedResult = sqrt preImage

    preImage_Rational = exactToRational preImage

    somehowRoundedResult_preImage :: RationalWithSpecials
    somehowRoundedResult_preImage
      = id
      $ liftUnary (\x -> x*x)
      $ exactToRational
      $ somehowRoundedResult

    -- this works because squareroot is monotonic
    wasRoundedCorrectly
      = somehowRoundedResult_preImage
      `sorted` preImage_Rational

    correctly_rounded_result
      = if (not $ isNaN somehowRoundedResult) || (isNegativeZero preImage)
          then
            if wasRoundedCorrectly
              then somehowRoundedResult
              else nudge somehowRoundedResult
          else
            somehowRoundedResult

    normal_case = assertNote note check correctly_rounded_result

    note = "something went wrong"
    check
      = True
      && we_rounded_correctly
      && we_rounded_tightly

    we_rounded_correctly = ((liftUnary (\x->x*x) $ exactToRational correctly_rounded_result) `sorted` (exactToRational preImage))
    we_rounded_tightly = (exactToRational preImage) `strict_sorted` (liftUnary (\x->x*x) $ exactToRational (the_other_nudge correctly_rounded_result))
      where
        strict_sorted x y = (x /= y) && (x `sorted` y)





convert
  ::
    ( Show a
    , Real a
    , RationalWithSpecialsLike a
    , IEEE_RoundingFuckUp_Correctable b
    )
  => Round
  -> a
  -> b
convert
  upOrDown
  integral
  = id
  $ result
--   $ sametype result
--   $ error ("debug caseee: " ++ show (integral, somehowRoundedResult, wasRoundedCorrectly, correctly_rounded_result))
  where
    result = normal_case

    sametype :: a -> a -> a
    sametype _ x = x

    sorted
      = liftComparatorConvert upOrDown
      $ roundingOrder_UnaryOperator upOrDown

    nudge = chooseNudging upOrDown

    the_other_nudge = chooseTheTheOtherNudging upOrDown

    argument_Rational = exactToRational $ integral

    somehowRoundedResult = exactFromRational $ argument_Rational

    somehow_rounded_result_Rational
      = exactToRational somehowRoundedResult

    wasRoundedCorrectly
      = somehow_rounded_result_Rational
      `sorted` argument_Rational

    correctly_rounded_result
      = if wasRoundedCorrectly
          then somehowRoundedResult
          else nudge somehowRoundedResult

    normal_case = assertNote note check correctly_rounded_result

    note = "something went wrong"
    check
      = True
      && we_rounded_correctly
      && we_rounded_tightly

    we_rounded_correctly = (Exisiting $ toRational correctly_rounded_result) `sorted` (Exisiting $ toRational integral)
    we_rounded_tightly = (Exisiting $ toRational integral) `strict_sorted` (exactToRational $ the_other_nudge correctly_rounded_result)
      where
        strict_sorted = liftComparator upOrDown $ \x y -> (x /= y) && (Exisiting x `sorted` Exisiting y)





i32_arithmic_signum_f64 :: Double -> Int32
i32_arithmic_signum_f64 (D# x) = I32# (intToInt32# (i32_arithmic_signum_f64# x))
i32_arithmic_signum_f32 :: Float -> Int32
i32_arithmic_signum_f32 (F# x) = I32# (intToInt32# (i32_arithmic_signum_f32# x))

-- https://stackoverflow.com/questions/1903954/is-there-a-standard-sign-function-signum-sgn-in-c-c
i32_arithmic_signum_f64# :: Double# -> Int#
i32_arithmic_signum_f64# x = (0.0## <## x) -# (x <## 0.0##)
i32_arithmic_signum_f32# :: Float# -> Int#
i32_arithmic_signum_f32# x = (0.0# `ltFloat#` x) -# (x `ltFloat#` 0.0#)



i32_sign_bit_f64 :: Double -> Int32
i32_sign_bit_f64 (D# x) = I32# (i32_sign_bit_f64# x)

i32_sign_bit_f64# :: Double# -> Int32#
i32_sign_bit_f64# value = result
  where
    value_as_word64 :: Word64#
    value_as_word64 = stgDoubleToWord64 value

    sign_bit :: Word64#
    sign_bit
      = 0b1000000000000000000000000000000000000000000000000000000000000000#Word64

    sign_bit_of_value :: Word64#
    sign_bit_of_value = and64# sign_bit value_as_word64

    result
      = case sign_bit_of_value of
          0b1000000000000000000000000000000000000000000000000000000000000000#Word64
            -> 0b1#Int32
          0b0000000000000000000000000000000000000000000000000000000000000000#Word64
            -> 0b0#Int32



i32_sign_bit_f32 :: Float -> Int32
i32_sign_bit_f32 (F# x) = I32# (i32_sign_bit_f32# x)

i32_sign_bit_f32# :: Float# -> Int32#
i32_sign_bit_f32# value = result
  where
    value_as_word32 :: Word32#
    value_as_word32 = stgFloatToWord32 value

    sign_bit :: Word32#
    sign_bit
      = 0b10000000000000000000000000000000#Word32

    sign_bit_of_value :: Word32#
    sign_bit_of_value = andWord32# sign_bit value_as_word32

    result
      = case sign_bit_of_value of
          0b10000000000000000000000000000000#Word32
            -> 0b1#Int32
          0b00000000000000000000000000000000#Word32
            -> 0b0#Int32

