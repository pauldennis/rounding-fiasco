
main = do
  mapM_ putStrLn specialization_variants_for_binary

  mapM_ putStrLn
    $ map specialization_variants_for_unary
    $ all_specialisation_variants

  putStrLn $ trailing_trimming "\
    \                                                        \n\
    \uint32_t i32_sign_bit_f64(double input)                 \n\
    \{                                                       \n\
    \  const uint32_t result = std::signbit(input);          \n\
    \  return result;                                        \n\
    \}                                                       \n\
    \uint32_t i32_sign_bit_f32(float input)                  \n\
    \{                                                       \n\
    \  const uint32_t result = i32_sign_bit_f64(input);      \n\
    \  return result;                                        \n\
    \}                                                       \n\
    \                                                        \n\
    \int32_t i32_arithmic_signum_f64(double input)           \n\
    \{                                                       \n\
    \  return (double(0) < input) - (input < double(0));     \n\
    \}                                                       \n\
    \int32_t i32_arithmic_signum_f32(float input)            \n\
    \{                                                       \n\
    \  return i32_arithmic_signum_f64(input);                \n\
    \}                                                       \n\
    \                                                        \n\
    \ "

trailing_trimming
  the_lines
  = id
  $ unlines
  $ map trimTrailingWhitespace
  $ lines
  $ the_lines
  where
    trimTrailingWhitespace
      line
      = id
      $ reverse
      $ dropWhile (==' ')
      $ reverse
      $ line


all_specialisation_variants = []
  ++ specialization_variants_for_unary_config_for_squareRoot
  ++ specialization_variants_for_unary_config_for_convert
  ++ specialization_variants_for_unary_config_for_demote


specialization_variants_for_unary_config_for_squareRoot = do
  input_type <- ["f32", "f64"]
  function_name <- ["squareRoot"]
  let output_type = input_type
  rounding_webassembly <- ["floor", "ceil", "truncate"]
  return (input_type, function_name, output_type, rounding_webassembly)

specialization_variants_for_unary_config_for_convert = do
  input_type <- do
    bitlength <- ["32", "64"]
    signedness <- ["unsigned", "signed"]
    return $ "i" ++ bitlength ++ "_" ++ signedness
  function_name <- ["convert"]
  output_type <- ["f32", "f64"]
  rounding_webassembly <- ["floor", "ceil", "truncate"]
  return (input_type, function_name, output_type, rounding_webassembly)

specialization_variants_for_unary_config_for_demote = do
  input_type <- ["f64"]
  function_name <- ["demote"]
  output_type <- ["f32"]
  rounding_webassembly <- ["floor", "ceil", "truncate"]
  return (input_type, function_name, output_type, rounding_webassembly)



specialization_variants_for_unary
  (input_type, function_name, output_type, rounding_webassembly)
  = id
  $ trailing_trimming
  $ x
  where
--   input_type = "i64_unsigned"
--   function_name = "convert"
--   output_type = "f64"
--   rounding_webassembly = "floor"

  optional_input_type
    = case function_name of
        "squareRoot" -> ""
        "demote" -> ""
        "convert" -> "_" ++ input_type

  input_type_cpp
    = case input_type of
        "i64_unsigned" -> "uint64_t"
        "i64_signed" -> "int64_t"
        "i32_unsigned" -> "uint32_t"
        "i32_signed" -> "int32_t"
        "f32" -> "float"
        "f64" -> "double"

  output_type_cpp
    = case output_type of
        "f64" -> "double"
        "f32" -> "float"

  rounding_mode_cpp
    = case rounding_webassembly of
        "floor" -> "FE_DOWNWARD"
        "ceil" -> "FE_UPWARD"
        "truncate" -> "FE_TOWARDZERO"

  function_cpp
    = case function_name of
        "convert" -> "static_cast<"++output_type_cpp++">"
        "demote" -> "static_cast<"++output_type_cpp++">"
        "squareRoot" -> "std::sqrt"

  x = "\
  \"++output_type_cpp++" "++output_type++"_"++function_name++optional_input_type++"_"++rounding_webassembly++"("++input_type_cpp++" input)    \n\
  \{                                                                                                                                          \n\
  \  #ifndef "++rounding_mode_cpp++"                                                                                                          \n\
  \    #error \""++rounding_mode_cpp++" not supported\"                                                                                       \n\
  \  #endif                                                                                                                                   \n\
  \                                                                                                                                           \n\
  \  const int originalRoundingMode = fegetround();                                                                                           \n\
  \  const int didItWork = fesetround( "++rounding_mode_cpp++" );                                                                             \n\
  \                                                                                                                                           \n\
  \  if ( 0 == didItWork )                                                                                                                    \n\
  \  {                                                                                                                                        \n\
  \    // it worked                                                                                                                           \n\
  \  }                                                                                                                                        \n\
  \  else                                                                                                                                     \n\
  \  {                                                                                                                                        \n\
  \    throw \"could not set rounding mode to "++rounding_mode_cpp++"\";                                                                      \n\
  \  }                                                                                                                                        \n\
  \                                                                                                                                           \n\
  \  const "++output_type_cpp++" result = "++function_cpp++"(input);                                                                          \n\
  \                                                                                                                                           \n\
  \  fesetround( originalRoundingMode );                                                                                                      \n\
  \                                                                                                                                           \n\
  \  return result;                                                                                                                           \n\
  \}                                                                                                                                          \n\
  \ "



specialization_variants_for_binary
  :: [String]
specialization_variants_for_binary = do
  floating_point_type <- ["f32", "f64"]
  binary_operation_name <- ["add", "subtract", "multiplicate", "divide"]
  rounding_webassembly <- ["floor", "ceil", "truncate"]
  return $ specialize_binary_operation floating_point_type binary_operation_name rounding_webassembly

specialize_binary_operation
  :: String
  -> String
  -> String
  -> String
specialize_binary_operation
  operator_type_webassembly
  binary_operation_name
  rounding_webassembly
  = id
  $ trailing_trimming
  $ x
  where
--   operator_type_webassembly = "f32"
--   binary_operation_name = "add"
--   rounding_webassembly = "floor"

  operator_type_cpp
    = case operator_type_webassembly of
        "f32" -> "float"
        "f64" -> "double"

  rounding_mode_cpp
    = case rounding_webassembly of
        "floor" -> "FE_DOWNWARD"
        "ceil" -> "FE_UPWARD"
        "truncate" -> "FE_TOWARDZERO"

  function_cpp
    = case binary_operation_name of
        "add" -> "std::plus<"++operator_type_cpp++">()"
        "subtract" -> "std::minus<"++operator_type_cpp++">()"
        "multiplicate" -> "std::multiplies<"++operator_type_cpp++">()"
        "divide" -> "std::divides<"++operator_type_cpp++">()"



  x = "\
  \\n\
  \\n\
  \#pragma STDC FENV_ACCESS ON\n\
  \                                                                                                                                                                                                 \n\
  \"++operator_type_cpp++" "++operator_type_webassembly++"_"++binary_operation_name++"_"++rounding_webassembly++"("++operator_type_cpp++" left, "++operator_type_cpp++" right)                      \n\
  \{                                                                                                                                                                                                \n\
  \  #ifndef "++rounding_mode_cpp++"                                                                                                                                                                \n\
  \    #error \""++rounding_mode_cpp++" not supported\"                                                                                                                                             \n\
  \  #endif                                                                                                                                                                                         \n\
  \                                                                                                                                                                                                 \n\
  \  const int originalRoundingMode = fegetround();                                                                                                                                                 \n\
  \  const int didItWork = fesetround( "++rounding_mode_cpp++" );                                                                                                                                   \n\
  \                                                                                                                                                                                                 \n\
  \  if ( 0 == didItWork )                                                                                                                                                                          \n\
  \  {                                                                                                                                                                                              \n\
  \    // it worked                                                                                                                                                                                 \n\
  \  }                                                                                                                                                                                              \n\
  \  else                                                                                                                                                                                           \n\
  \  {                                                                                                                                                                                              \n\
  \    throw \"could not set rounding mode to "++rounding_mode_cpp++"\";                                                                                                                            \n\
  \  }                                                                                                                                                                                              \n\
  \                                                                                                                                                                                                 \n\
  \  const float result = "++function_cpp++"(left, right);                                                                                                                                          \n\
  \                                                                                                                                                                                                 \n\
  \  fesetround( originalRoundingMode );                                                                                                                                                            \n\
  \                                                                                                                                                                                                 \n\
  \  return result;                                                                                                                                                                                 \n\
  \}                                                                                                                                                                                                \n\
  \\n\
  \ "

