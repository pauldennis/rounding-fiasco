set -e

# git clean -dfX

function remove_memory_export()
{
  wasm2wat $1 -o tmp
  sed -i 's/(export "memory" (memory 0))/;; export removed/g' tmp
  wat2wasm tmp -o $1
}

function unsing_imports_is_undefined()
{
  wasm2wat $1 -o tmp
  sed -i 's/call 0$/unreachable ;; no import/g' tmp   && echo -n "."
  sed -i 's/call 1$/unreachable ;; no import/g' tmp   && echo -n "."
  sed -i 's/call 2$/unreachable ;; no import/g' tmp   && echo -n "."
  sed -i 's/call 3$/unreachable ;; no import/g' tmp   && echo -n "."
  sed -i 's/call 4$/unreachable ;; no import/g' tmp   && echo -n "."
  sed -i 's/call 5$/unreachable ;; no import/g' tmp   && echo -n "."
  sed -i 's/call 6$/unreachable ;; no import/g' tmp   && echo -n "."
  sed -i 's/call 7$/unreachable ;; no import/g' tmp   && echo -n "."
  sed -i 's/call 8$/unreachable ;; no import/g' tmp   && echo -n "."
  sed -i 's/call 9$/unreachable ;; no import/g' tmp   && echo -n "."
  sed -i 's/call 10$/unreachable ;; no import/g' tmp  && echo -n "."
  sed -i 's/call 11$/unreachable ;; no import/g' tmp  && echo -n "."
  sed -i 's/call 12$/unreachable ;; no import/g' tmp  && echo -n "."
  sed -i 's/call 13$/unreachable ;; no import/g' tmp  && echo -n "."
  sed -i 's/call 14$/unreachable ;; no import/g' tmp  && echo -n "."
  sed -i 's/call 15$/unreachable ;; no import/g' tmp  && echo -n "."
  sed -i 's/call 16$/unreachable ;; no import/g' tmp  && echo -n "."
  sed -i 's/call 17$/unreachable ;; no import/g' tmp  && echo -n "."
  echo
  wat2wasm tmp -o $1
}


rounding_exports="--export=f32_squareRoot_ceil,--export=f32_squareRoot_floor,--export=f32_squareRoot_truncate,--export=f64_squareRoot_ceil,--export=f64_squareRoot_floor,--export=f64_squareRoot_truncate,--export=f32_add_ceil,--export=f32_add_floor,--export=f32_add_truncate,--export=f32_subtract_ceil,--export=f32_subtract_floor,--export=f32_subtract_truncate,--export=f32_multiplicate_ceil,--export=f32_multiplicate_floor,--export=f32_multiplicate_truncate,--export=f32_divide_ceil,--export=f32_divide_floor,--export=f32_divide_truncate,--export=f64_add_ceil,--export=f64_add_floor,--export=f64_add_truncate,--export=f64_subtract_ceil,--export=f64_subtract_floor,--export=f64_subtract_truncate,--export=f64_multiplicate_ceil,--export=f64_multiplicate_floor,--export=f64_multiplicate_truncate,--export=f64_divide_ceil,--export=f64_divide_floor,--export=f64_divide_truncate,--export=f32_convert_i32_unsigned_ceil,--export=f32_convert_i32_signed_ceil,--export=f32_convert_i32_unsigned_floor,--export=f32_convert_i32_signed_floor,--export=f32_convert_i32_unsigned_truncate,--export=f32_convert_i32_signed_truncate,--export=f32_convert_i64_unsigned_ceil,--export=f32_convert_i64_signed_ceil,--export=f32_convert_i64_unsigned_floor,--export=f32_convert_i64_signed_floor,--export=f32_convert_i64_unsigned_truncate,--export=f32_convert_i64_signed_truncate,--export=f64_convert_i32_unsigned_ceil,--export=f64_convert_i32_signed_ceil,--export=f64_convert_i32_unsigned_floor,--export=f64_convert_i32_signed_floor,--export=f64_convert_i32_unsigned_truncate,--export=f64_convert_i32_signed_truncate,--export=f64_convert_i64_unsigned_ceil,--export=f64_convert_i64_signed_ceil,--export=f64_convert_i64_unsigned_floor,--export=f64_convert_i64_signed_floor,--export=f64_convert_i64_unsigned_truncate,--export=f64_convert_i64_signed_truncate,--export=i32_arithmic_signum_f64,--export=i32_arithmic_signum_f32,--export=i32_sign_bit_f64,--export=i32_sign_bit_f32,--export=f32_demote_f64_floor,--export=f32_demote_f64_ceil,--export=f32_demote_f64_truncate"

wasm32-wasi-ghc \
  RoundingFiasco.hs \
  -O2 \
  -o RoundingFiasco_pre.wasm \
  -no-hs-main \
  -optl-mexec-model=reactor \
  -optl-Wl,--export=hs_init,--export=testcase,${rounding_exports}

wasm32-wasi-ghc \
  RoundingFiasco.hs \
  pre_initialize.c \
  -O2 \
  -o RoundingFiasco_pre.wasm \
  -no-hs-main \
  -optl-mexec-model=reactor \
  -optl-Wl,--export=testcase,${rounding_exports}

wizer \
  --allow-wasi \
  --wasm-bulk-memory true \
  RoundingFiasco_pre.wasm -o \
  RoundingFiasco.wizer.wasm

cp RoundingFiasco.wizer.wasm RoundingFiasco.wasm
wasm2wat RoundingFiasco.wasm > pre_opt_look.wat

echo "replace hackery"

remove_memory_export RoundingFiasco.wizer.wasm
unsing_imports_is_undefined RoundingFiasco.wizer.wasm

echo "wasm-opt"

wasm-opt \
  --enable-multivalue \
  --enable-bulk-memory \
  --enable-simd \
  --enable-reference-types \
  --enable-nontrapping-float-to-int \
  --enable-sign-ext \
  --enable-mutable-globals \
  -O4 \
  --traps-never-happen \
  --generate-global-effects \
  --remove-imports \
  --remove-unused-module-elements \
  --monomorphize \
  --dae-optimizing \
  --partial-inlining-ifs 1000000 \
  RoundingFiasco.wizer.wasm \
  -o RoundingFiasco-Release.wasm \


wasm2wat RoundingFiasco-Release.wasm > look.wat

echo "begin test"

time ./testing.sh
