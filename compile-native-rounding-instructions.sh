set -e

runhaskell RoundingModeRoundingInstructionGenerator.hs > instructionsUsingRoundingMode.cpp

clang++ roundingModeRounding.cpp -o roundingModeRounding && ./roundingModeRounding
