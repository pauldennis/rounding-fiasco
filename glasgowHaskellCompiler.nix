{ sources ? import ./nix/sources.nix
, pkgs ? import sources.nixpkgs {}

, ghc ? pkgs.haskellPackages.ghcWithPackages (pkgs: with pkgs; [
    binary
  ])

}:

ghc
