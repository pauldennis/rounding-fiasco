

#pragma STDC FENV_ACCESS ON

float f32_add_floor(float left, float right)
{
  #ifndef FE_DOWNWARD
    #error "FE_DOWNWARD not supported"
  #endif

  const int originalRoundingMode = fegetround();
  const int didItWork = fesetround( FE_DOWNWARD );

  if ( 0 == didItWork )
  {
    // it worked
  }
  else
  {
    throw "could not set rounding mode to FE_DOWNWARD";
  }

  const float result = std::plus<float>()(left, right);

  fesetround( originalRoundingMode );

  return result;
}





#pragma STDC FENV_ACCESS ON

float f32_add_ceil(float left, float right)
{
  #ifndef FE_UPWARD
    #error "FE_UPWARD not supported"
  #endif

  const int originalRoundingMode = fegetround();
  const int didItWork = fesetround( FE_UPWARD );

  if ( 0 == didItWork )
  {
    // it worked
  }
  else
  {
    throw "could not set rounding mode to FE_UPWARD";
  }

  const float result = std::plus<float>()(left, right);

  fesetround( originalRoundingMode );

  return result;
}





#pragma STDC FENV_ACCESS ON

float f32_add_truncate(float left, float right)
{
  #ifndef FE_TOWARDZERO
    #error "FE_TOWARDZERO not supported"
  #endif

  const int originalRoundingMode = fegetround();
  const int didItWork = fesetround( FE_TOWARDZERO );

  if ( 0 == didItWork )
  {
    // it worked
  }
  else
  {
    throw "could not set rounding mode to FE_TOWARDZERO";
  }

  const float result = std::plus<float>()(left, right);

  fesetround( originalRoundingMode );

  return result;
}





#pragma STDC FENV_ACCESS ON

float f32_subtract_floor(float left, float right)
{
  #ifndef FE_DOWNWARD
    #error "FE_DOWNWARD not supported"
  #endif

  const int originalRoundingMode = fegetround();
  const int didItWork = fesetround( FE_DOWNWARD );

  if ( 0 == didItWork )
  {
    // it worked
  }
  else
  {
    throw "could not set rounding mode to FE_DOWNWARD";
  }

  const float result = std::minus<float>()(left, right);

  fesetround( originalRoundingMode );

  return result;
}





#pragma STDC FENV_ACCESS ON

float f32_subtract_ceil(float left, float right)
{
  #ifndef FE_UPWARD
    #error "FE_UPWARD not supported"
  #endif

  const int originalRoundingMode = fegetround();
  const int didItWork = fesetround( FE_UPWARD );

  if ( 0 == didItWork )
  {
    // it worked
  }
  else
  {
    throw "could not set rounding mode to FE_UPWARD";
  }

  const float result = std::minus<float>()(left, right);

  fesetround( originalRoundingMode );

  return result;
}





#pragma STDC FENV_ACCESS ON

float f32_subtract_truncate(float left, float right)
{
  #ifndef FE_TOWARDZERO
    #error "FE_TOWARDZERO not supported"
  #endif

  const int originalRoundingMode = fegetround();
  const int didItWork = fesetround( FE_TOWARDZERO );

  if ( 0 == didItWork )
  {
    // it worked
  }
  else
  {
    throw "could not set rounding mode to FE_TOWARDZERO";
  }

  const float result = std::minus<float>()(left, right);

  fesetround( originalRoundingMode );

  return result;
}





#pragma STDC FENV_ACCESS ON

float f32_multiplicate_floor(float left, float right)
{
  #ifndef FE_DOWNWARD
    #error "FE_DOWNWARD not supported"
  #endif

  const int originalRoundingMode = fegetround();
  const int didItWork = fesetround( FE_DOWNWARD );

  if ( 0 == didItWork )
  {
    // it worked
  }
  else
  {
    throw "could not set rounding mode to FE_DOWNWARD";
  }

  const float result = std::multiplies<float>()(left, right);

  fesetround( originalRoundingMode );

  return result;
}





#pragma STDC FENV_ACCESS ON

float f32_multiplicate_ceil(float left, float right)
{
  #ifndef FE_UPWARD
    #error "FE_UPWARD not supported"
  #endif

  const int originalRoundingMode = fegetround();
  const int didItWork = fesetround( FE_UPWARD );

  if ( 0 == didItWork )
  {
    // it worked
  }
  else
  {
    throw "could not set rounding mode to FE_UPWARD";
  }

  const float result = std::multiplies<float>()(left, right);

  fesetround( originalRoundingMode );

  return result;
}





#pragma STDC FENV_ACCESS ON

float f32_multiplicate_truncate(float left, float right)
{
  #ifndef FE_TOWARDZERO
    #error "FE_TOWARDZERO not supported"
  #endif

  const int originalRoundingMode = fegetround();
  const int didItWork = fesetround( FE_TOWARDZERO );

  if ( 0 == didItWork )
  {
    // it worked
  }
  else
  {
    throw "could not set rounding mode to FE_TOWARDZERO";
  }

  const float result = std::multiplies<float>()(left, right);

  fesetround( originalRoundingMode );

  return result;
}





#pragma STDC FENV_ACCESS ON

float f32_divide_floor(float left, float right)
{
  #ifndef FE_DOWNWARD
    #error "FE_DOWNWARD not supported"
  #endif

  const int originalRoundingMode = fegetround();
  const int didItWork = fesetround( FE_DOWNWARD );

  if ( 0 == didItWork )
  {
    // it worked
  }
  else
  {
    throw "could not set rounding mode to FE_DOWNWARD";
  }

  const float result = std::divides<float>()(left, right);

  fesetround( originalRoundingMode );

  return result;
}





#pragma STDC FENV_ACCESS ON

float f32_divide_ceil(float left, float right)
{
  #ifndef FE_UPWARD
    #error "FE_UPWARD not supported"
  #endif

  const int originalRoundingMode = fegetround();
  const int didItWork = fesetround( FE_UPWARD );

  if ( 0 == didItWork )
  {
    // it worked
  }
  else
  {
    throw "could not set rounding mode to FE_UPWARD";
  }

  const float result = std::divides<float>()(left, right);

  fesetround( originalRoundingMode );

  return result;
}





#pragma STDC FENV_ACCESS ON

float f32_divide_truncate(float left, float right)
{
  #ifndef FE_TOWARDZERO
    #error "FE_TOWARDZERO not supported"
  #endif

  const int originalRoundingMode = fegetround();
  const int didItWork = fesetround( FE_TOWARDZERO );

  if ( 0 == didItWork )
  {
    // it worked
  }
  else
  {
    throw "could not set rounding mode to FE_TOWARDZERO";
  }

  const float result = std::divides<float>()(left, right);

  fesetround( originalRoundingMode );

  return result;
}





#pragma STDC FENV_ACCESS ON

double f64_add_floor(double left, double right)
{
  #ifndef FE_DOWNWARD
    #error "FE_DOWNWARD not supported"
  #endif

  const int originalRoundingMode = fegetround();
  const int didItWork = fesetround( FE_DOWNWARD );

  if ( 0 == didItWork )
  {
    // it worked
  }
  else
  {
    throw "could not set rounding mode to FE_DOWNWARD";
  }

  const float result = std::plus<double>()(left, right);

  fesetround( originalRoundingMode );

  return result;
}





#pragma STDC FENV_ACCESS ON

double f64_add_ceil(double left, double right)
{
  #ifndef FE_UPWARD
    #error "FE_UPWARD not supported"
  #endif

  const int originalRoundingMode = fegetround();
  const int didItWork = fesetround( FE_UPWARD );

  if ( 0 == didItWork )
  {
    // it worked
  }
  else
  {
    throw "could not set rounding mode to FE_UPWARD";
  }

  const float result = std::plus<double>()(left, right);

  fesetround( originalRoundingMode );

  return result;
}





#pragma STDC FENV_ACCESS ON

double f64_add_truncate(double left, double right)
{
  #ifndef FE_TOWARDZERO
    #error "FE_TOWARDZERO not supported"
  #endif

  const int originalRoundingMode = fegetround();
  const int didItWork = fesetround( FE_TOWARDZERO );

  if ( 0 == didItWork )
  {
    // it worked
  }
  else
  {
    throw "could not set rounding mode to FE_TOWARDZERO";
  }

  const float result = std::plus<double>()(left, right);

  fesetround( originalRoundingMode );

  return result;
}





#pragma STDC FENV_ACCESS ON

double f64_subtract_floor(double left, double right)
{
  #ifndef FE_DOWNWARD
    #error "FE_DOWNWARD not supported"
  #endif

  const int originalRoundingMode = fegetround();
  const int didItWork = fesetround( FE_DOWNWARD );

  if ( 0 == didItWork )
  {
    // it worked
  }
  else
  {
    throw "could not set rounding mode to FE_DOWNWARD";
  }

  const float result = std::minus<double>()(left, right);

  fesetround( originalRoundingMode );

  return result;
}





#pragma STDC FENV_ACCESS ON

double f64_subtract_ceil(double left, double right)
{
  #ifndef FE_UPWARD
    #error "FE_UPWARD not supported"
  #endif

  const int originalRoundingMode = fegetround();
  const int didItWork = fesetround( FE_UPWARD );

  if ( 0 == didItWork )
  {
    // it worked
  }
  else
  {
    throw "could not set rounding mode to FE_UPWARD";
  }

  const float result = std::minus<double>()(left, right);

  fesetround( originalRoundingMode );

  return result;
}





#pragma STDC FENV_ACCESS ON

double f64_subtract_truncate(double left, double right)
{
  #ifndef FE_TOWARDZERO
    #error "FE_TOWARDZERO not supported"
  #endif

  const int originalRoundingMode = fegetround();
  const int didItWork = fesetround( FE_TOWARDZERO );

  if ( 0 == didItWork )
  {
    // it worked
  }
  else
  {
    throw "could not set rounding mode to FE_TOWARDZERO";
  }

  const float result = std::minus<double>()(left, right);

  fesetround( originalRoundingMode );

  return result;
}





#pragma STDC FENV_ACCESS ON

double f64_multiplicate_floor(double left, double right)
{
  #ifndef FE_DOWNWARD
    #error "FE_DOWNWARD not supported"
  #endif

  const int originalRoundingMode = fegetround();
  const int didItWork = fesetround( FE_DOWNWARD );

  if ( 0 == didItWork )
  {
    // it worked
  }
  else
  {
    throw "could not set rounding mode to FE_DOWNWARD";
  }

  const float result = std::multiplies<double>()(left, right);

  fesetround( originalRoundingMode );

  return result;
}





#pragma STDC FENV_ACCESS ON

double f64_multiplicate_ceil(double left, double right)
{
  #ifndef FE_UPWARD
    #error "FE_UPWARD not supported"
  #endif

  const int originalRoundingMode = fegetround();
  const int didItWork = fesetround( FE_UPWARD );

  if ( 0 == didItWork )
  {
    // it worked
  }
  else
  {
    throw "could not set rounding mode to FE_UPWARD";
  }

  const float result = std::multiplies<double>()(left, right);

  fesetround( originalRoundingMode );

  return result;
}





#pragma STDC FENV_ACCESS ON

double f64_multiplicate_truncate(double left, double right)
{
  #ifndef FE_TOWARDZERO
    #error "FE_TOWARDZERO not supported"
  #endif

  const int originalRoundingMode = fegetround();
  const int didItWork = fesetround( FE_TOWARDZERO );

  if ( 0 == didItWork )
  {
    // it worked
  }
  else
  {
    throw "could not set rounding mode to FE_TOWARDZERO";
  }

  const float result = std::multiplies<double>()(left, right);

  fesetround( originalRoundingMode );

  return result;
}





#pragma STDC FENV_ACCESS ON

double f64_divide_floor(double left, double right)
{
  #ifndef FE_DOWNWARD
    #error "FE_DOWNWARD not supported"
  #endif

  const int originalRoundingMode = fegetround();
  const int didItWork = fesetround( FE_DOWNWARD );

  if ( 0 == didItWork )
  {
    // it worked
  }
  else
  {
    throw "could not set rounding mode to FE_DOWNWARD";
  }

  const float result = std::divides<double>()(left, right);

  fesetround( originalRoundingMode );

  return result;
}





#pragma STDC FENV_ACCESS ON

double f64_divide_ceil(double left, double right)
{
  #ifndef FE_UPWARD
    #error "FE_UPWARD not supported"
  #endif

  const int originalRoundingMode = fegetround();
  const int didItWork = fesetround( FE_UPWARD );

  if ( 0 == didItWork )
  {
    // it worked
  }
  else
  {
    throw "could not set rounding mode to FE_UPWARD";
  }

  const float result = std::divides<double>()(left, right);

  fesetround( originalRoundingMode );

  return result;
}





#pragma STDC FENV_ACCESS ON

double f64_divide_truncate(double left, double right)
{
  #ifndef FE_TOWARDZERO
    #error "FE_TOWARDZERO not supported"
  #endif

  const int originalRoundingMode = fegetround();
  const int didItWork = fesetround( FE_TOWARDZERO );

  if ( 0 == didItWork )
  {
    // it worked
  }
  else
  {
    throw "could not set rounding mode to FE_TOWARDZERO";
  }

  const float result = std::divides<double>()(left, right);

  fesetround( originalRoundingMode );

  return result;
}



float f32_squareRoot_floor(float input)
{
  #ifndef FE_DOWNWARD
    #error "FE_DOWNWARD not supported"
  #endif

  const int originalRoundingMode = fegetround();
  const int didItWork = fesetround( FE_DOWNWARD );

  if ( 0 == didItWork )
  {
    // it worked
  }
  else
  {
    throw "could not set rounding mode to FE_DOWNWARD";
  }

  const float result = std::sqrt(input);

  fesetround( originalRoundingMode );

  return result;
}


float f32_squareRoot_ceil(float input)
{
  #ifndef FE_UPWARD
    #error "FE_UPWARD not supported"
  #endif

  const int originalRoundingMode = fegetround();
  const int didItWork = fesetround( FE_UPWARD );

  if ( 0 == didItWork )
  {
    // it worked
  }
  else
  {
    throw "could not set rounding mode to FE_UPWARD";
  }

  const float result = std::sqrt(input);

  fesetround( originalRoundingMode );

  return result;
}


float f32_squareRoot_truncate(float input)
{
  #ifndef FE_TOWARDZERO
    #error "FE_TOWARDZERO not supported"
  #endif

  const int originalRoundingMode = fegetround();
  const int didItWork = fesetround( FE_TOWARDZERO );

  if ( 0 == didItWork )
  {
    // it worked
  }
  else
  {
    throw "could not set rounding mode to FE_TOWARDZERO";
  }

  const float result = std::sqrt(input);

  fesetround( originalRoundingMode );

  return result;
}


double f64_squareRoot_floor(double input)
{
  #ifndef FE_DOWNWARD
    #error "FE_DOWNWARD not supported"
  #endif

  const int originalRoundingMode = fegetround();
  const int didItWork = fesetround( FE_DOWNWARD );

  if ( 0 == didItWork )
  {
    // it worked
  }
  else
  {
    throw "could not set rounding mode to FE_DOWNWARD";
  }

  const double result = std::sqrt(input);

  fesetround( originalRoundingMode );

  return result;
}


double f64_squareRoot_ceil(double input)
{
  #ifndef FE_UPWARD
    #error "FE_UPWARD not supported"
  #endif

  const int originalRoundingMode = fegetround();
  const int didItWork = fesetround( FE_UPWARD );

  if ( 0 == didItWork )
  {
    // it worked
  }
  else
  {
    throw "could not set rounding mode to FE_UPWARD";
  }

  const double result = std::sqrt(input);

  fesetround( originalRoundingMode );

  return result;
}


double f64_squareRoot_truncate(double input)
{
  #ifndef FE_TOWARDZERO
    #error "FE_TOWARDZERO not supported"
  #endif

  const int originalRoundingMode = fegetround();
  const int didItWork = fesetround( FE_TOWARDZERO );

  if ( 0 == didItWork )
  {
    // it worked
  }
  else
  {
    throw "could not set rounding mode to FE_TOWARDZERO";
  }

  const double result = std::sqrt(input);

  fesetround( originalRoundingMode );

  return result;
}


float f32_convert_i32_unsigned_floor(uint32_t input)
{
  #ifndef FE_DOWNWARD
    #error "FE_DOWNWARD not supported"
  #endif

  const int originalRoundingMode = fegetround();
  const int didItWork = fesetround( FE_DOWNWARD );

  if ( 0 == didItWork )
  {
    // it worked
  }
  else
  {
    throw "could not set rounding mode to FE_DOWNWARD";
  }

  const float result = static_cast<float>(input);

  fesetround( originalRoundingMode );

  return result;
}


float f32_convert_i32_unsigned_ceil(uint32_t input)
{
  #ifndef FE_UPWARD
    #error "FE_UPWARD not supported"
  #endif

  const int originalRoundingMode = fegetround();
  const int didItWork = fesetround( FE_UPWARD );

  if ( 0 == didItWork )
  {
    // it worked
  }
  else
  {
    throw "could not set rounding mode to FE_UPWARD";
  }

  const float result = static_cast<float>(input);

  fesetround( originalRoundingMode );

  return result;
}


float f32_convert_i32_unsigned_truncate(uint32_t input)
{
  #ifndef FE_TOWARDZERO
    #error "FE_TOWARDZERO not supported"
  #endif

  const int originalRoundingMode = fegetround();
  const int didItWork = fesetround( FE_TOWARDZERO );

  if ( 0 == didItWork )
  {
    // it worked
  }
  else
  {
    throw "could not set rounding mode to FE_TOWARDZERO";
  }

  const float result = static_cast<float>(input);

  fesetround( originalRoundingMode );

  return result;
}


double f64_convert_i32_unsigned_floor(uint32_t input)
{
  #ifndef FE_DOWNWARD
    #error "FE_DOWNWARD not supported"
  #endif

  const int originalRoundingMode = fegetround();
  const int didItWork = fesetround( FE_DOWNWARD );

  if ( 0 == didItWork )
  {
    // it worked
  }
  else
  {
    throw "could not set rounding mode to FE_DOWNWARD";
  }

  const double result = static_cast<double>(input);

  fesetround( originalRoundingMode );

  return result;
}


double f64_convert_i32_unsigned_ceil(uint32_t input)
{
  #ifndef FE_UPWARD
    #error "FE_UPWARD not supported"
  #endif

  const int originalRoundingMode = fegetround();
  const int didItWork = fesetround( FE_UPWARD );

  if ( 0 == didItWork )
  {
    // it worked
  }
  else
  {
    throw "could not set rounding mode to FE_UPWARD";
  }

  const double result = static_cast<double>(input);

  fesetround( originalRoundingMode );

  return result;
}


double f64_convert_i32_unsigned_truncate(uint32_t input)
{
  #ifndef FE_TOWARDZERO
    #error "FE_TOWARDZERO not supported"
  #endif

  const int originalRoundingMode = fegetround();
  const int didItWork = fesetround( FE_TOWARDZERO );

  if ( 0 == didItWork )
  {
    // it worked
  }
  else
  {
    throw "could not set rounding mode to FE_TOWARDZERO";
  }

  const double result = static_cast<double>(input);

  fesetround( originalRoundingMode );

  return result;
}


float f32_convert_i32_signed_floor(int32_t input)
{
  #ifndef FE_DOWNWARD
    #error "FE_DOWNWARD not supported"
  #endif

  const int originalRoundingMode = fegetround();
  const int didItWork = fesetround( FE_DOWNWARD );

  if ( 0 == didItWork )
  {
    // it worked
  }
  else
  {
    throw "could not set rounding mode to FE_DOWNWARD";
  }

  const float result = static_cast<float>(input);

  fesetround( originalRoundingMode );

  return result;
}


float f32_convert_i32_signed_ceil(int32_t input)
{
  #ifndef FE_UPWARD
    #error "FE_UPWARD not supported"
  #endif

  const int originalRoundingMode = fegetround();
  const int didItWork = fesetround( FE_UPWARD );

  if ( 0 == didItWork )
  {
    // it worked
  }
  else
  {
    throw "could not set rounding mode to FE_UPWARD";
  }

  const float result = static_cast<float>(input);

  fesetround( originalRoundingMode );

  return result;
}


float f32_convert_i32_signed_truncate(int32_t input)
{
  #ifndef FE_TOWARDZERO
    #error "FE_TOWARDZERO not supported"
  #endif

  const int originalRoundingMode = fegetround();
  const int didItWork = fesetround( FE_TOWARDZERO );

  if ( 0 == didItWork )
  {
    // it worked
  }
  else
  {
    throw "could not set rounding mode to FE_TOWARDZERO";
  }

  const float result = static_cast<float>(input);

  fesetround( originalRoundingMode );

  return result;
}


double f64_convert_i32_signed_floor(int32_t input)
{
  #ifndef FE_DOWNWARD
    #error "FE_DOWNWARD not supported"
  #endif

  const int originalRoundingMode = fegetround();
  const int didItWork = fesetround( FE_DOWNWARD );

  if ( 0 == didItWork )
  {
    // it worked
  }
  else
  {
    throw "could not set rounding mode to FE_DOWNWARD";
  }

  const double result = static_cast<double>(input);

  fesetround( originalRoundingMode );

  return result;
}


double f64_convert_i32_signed_ceil(int32_t input)
{
  #ifndef FE_UPWARD
    #error "FE_UPWARD not supported"
  #endif

  const int originalRoundingMode = fegetround();
  const int didItWork = fesetround( FE_UPWARD );

  if ( 0 == didItWork )
  {
    // it worked
  }
  else
  {
    throw "could not set rounding mode to FE_UPWARD";
  }

  const double result = static_cast<double>(input);

  fesetround( originalRoundingMode );

  return result;
}


double f64_convert_i32_signed_truncate(int32_t input)
{
  #ifndef FE_TOWARDZERO
    #error "FE_TOWARDZERO not supported"
  #endif

  const int originalRoundingMode = fegetround();
  const int didItWork = fesetround( FE_TOWARDZERO );

  if ( 0 == didItWork )
  {
    // it worked
  }
  else
  {
    throw "could not set rounding mode to FE_TOWARDZERO";
  }

  const double result = static_cast<double>(input);

  fesetround( originalRoundingMode );

  return result;
}


float f32_convert_i64_unsigned_floor(uint64_t input)
{
  #ifndef FE_DOWNWARD
    #error "FE_DOWNWARD not supported"
  #endif

  const int originalRoundingMode = fegetround();
  const int didItWork = fesetround( FE_DOWNWARD );

  if ( 0 == didItWork )
  {
    // it worked
  }
  else
  {
    throw "could not set rounding mode to FE_DOWNWARD";
  }

  const float result = static_cast<float>(input);

  fesetround( originalRoundingMode );

  return result;
}


float f32_convert_i64_unsigned_ceil(uint64_t input)
{
  #ifndef FE_UPWARD
    #error "FE_UPWARD not supported"
  #endif

  const int originalRoundingMode = fegetround();
  const int didItWork = fesetround( FE_UPWARD );

  if ( 0 == didItWork )
  {
    // it worked
  }
  else
  {
    throw "could not set rounding mode to FE_UPWARD";
  }

  const float result = static_cast<float>(input);

  fesetround( originalRoundingMode );

  return result;
}


float f32_convert_i64_unsigned_truncate(uint64_t input)
{
  #ifndef FE_TOWARDZERO
    #error "FE_TOWARDZERO not supported"
  #endif

  const int originalRoundingMode = fegetround();
  const int didItWork = fesetround( FE_TOWARDZERO );

  if ( 0 == didItWork )
  {
    // it worked
  }
  else
  {
    throw "could not set rounding mode to FE_TOWARDZERO";
  }

  const float result = static_cast<float>(input);

  fesetround( originalRoundingMode );

  return result;
}


double f64_convert_i64_unsigned_floor(uint64_t input)
{
  #ifndef FE_DOWNWARD
    #error "FE_DOWNWARD not supported"
  #endif

  const int originalRoundingMode = fegetround();
  const int didItWork = fesetround( FE_DOWNWARD );

  if ( 0 == didItWork )
  {
    // it worked
  }
  else
  {
    throw "could not set rounding mode to FE_DOWNWARD";
  }

  const double result = static_cast<double>(input);

  fesetround( originalRoundingMode );

  return result;
}


double f64_convert_i64_unsigned_ceil(uint64_t input)
{
  #ifndef FE_UPWARD
    #error "FE_UPWARD not supported"
  #endif

  const int originalRoundingMode = fegetround();
  const int didItWork = fesetround( FE_UPWARD );

  if ( 0 == didItWork )
  {
    // it worked
  }
  else
  {
    throw "could not set rounding mode to FE_UPWARD";
  }

  const double result = static_cast<double>(input);

  fesetround( originalRoundingMode );

  return result;
}


double f64_convert_i64_unsigned_truncate(uint64_t input)
{
  #ifndef FE_TOWARDZERO
    #error "FE_TOWARDZERO not supported"
  #endif

  const int originalRoundingMode = fegetround();
  const int didItWork = fesetround( FE_TOWARDZERO );

  if ( 0 == didItWork )
  {
    // it worked
  }
  else
  {
    throw "could not set rounding mode to FE_TOWARDZERO";
  }

  const double result = static_cast<double>(input);

  fesetround( originalRoundingMode );

  return result;
}


float f32_convert_i64_signed_floor(int64_t input)
{
  #ifndef FE_DOWNWARD
    #error "FE_DOWNWARD not supported"
  #endif

  const int originalRoundingMode = fegetround();
  const int didItWork = fesetround( FE_DOWNWARD );

  if ( 0 == didItWork )
  {
    // it worked
  }
  else
  {
    throw "could not set rounding mode to FE_DOWNWARD";
  }

  const float result = static_cast<float>(input);

  fesetround( originalRoundingMode );

  return result;
}


float f32_convert_i64_signed_ceil(int64_t input)
{
  #ifndef FE_UPWARD
    #error "FE_UPWARD not supported"
  #endif

  const int originalRoundingMode = fegetround();
  const int didItWork = fesetround( FE_UPWARD );

  if ( 0 == didItWork )
  {
    // it worked
  }
  else
  {
    throw "could not set rounding mode to FE_UPWARD";
  }

  const float result = static_cast<float>(input);

  fesetround( originalRoundingMode );

  return result;
}


float f32_convert_i64_signed_truncate(int64_t input)
{
  #ifndef FE_TOWARDZERO
    #error "FE_TOWARDZERO not supported"
  #endif

  const int originalRoundingMode = fegetround();
  const int didItWork = fesetround( FE_TOWARDZERO );

  if ( 0 == didItWork )
  {
    // it worked
  }
  else
  {
    throw "could not set rounding mode to FE_TOWARDZERO";
  }

  const float result = static_cast<float>(input);

  fesetround( originalRoundingMode );

  return result;
}


double f64_convert_i64_signed_floor(int64_t input)
{
  #ifndef FE_DOWNWARD
    #error "FE_DOWNWARD not supported"
  #endif

  const int originalRoundingMode = fegetround();
  const int didItWork = fesetround( FE_DOWNWARD );

  if ( 0 == didItWork )
  {
    // it worked
  }
  else
  {
    throw "could not set rounding mode to FE_DOWNWARD";
  }

  const double result = static_cast<double>(input);

  fesetround( originalRoundingMode );

  return result;
}


double f64_convert_i64_signed_ceil(int64_t input)
{
  #ifndef FE_UPWARD
    #error "FE_UPWARD not supported"
  #endif

  const int originalRoundingMode = fegetround();
  const int didItWork = fesetround( FE_UPWARD );

  if ( 0 == didItWork )
  {
    // it worked
  }
  else
  {
    throw "could not set rounding mode to FE_UPWARD";
  }

  const double result = static_cast<double>(input);

  fesetround( originalRoundingMode );

  return result;
}


double f64_convert_i64_signed_truncate(int64_t input)
{
  #ifndef FE_TOWARDZERO
    #error "FE_TOWARDZERO not supported"
  #endif

  const int originalRoundingMode = fegetround();
  const int didItWork = fesetround( FE_TOWARDZERO );

  if ( 0 == didItWork )
  {
    // it worked
  }
  else
  {
    throw "could not set rounding mode to FE_TOWARDZERO";
  }

  const double result = static_cast<double>(input);

  fesetround( originalRoundingMode );

  return result;
}


float f32_demote_floor(double input)
{
  #ifndef FE_DOWNWARD
    #error "FE_DOWNWARD not supported"
  #endif

  const int originalRoundingMode = fegetround();
  const int didItWork = fesetround( FE_DOWNWARD );

  if ( 0 == didItWork )
  {
    // it worked
  }
  else
  {
    throw "could not set rounding mode to FE_DOWNWARD";
  }

  const float result = static_cast<float>(input);

  fesetround( originalRoundingMode );

  return result;
}


float f32_demote_ceil(double input)
{
  #ifndef FE_UPWARD
    #error "FE_UPWARD not supported"
  #endif

  const int originalRoundingMode = fegetround();
  const int didItWork = fesetround( FE_UPWARD );

  if ( 0 == didItWork )
  {
    // it worked
  }
  else
  {
    throw "could not set rounding mode to FE_UPWARD";
  }

  const float result = static_cast<float>(input);

  fesetround( originalRoundingMode );

  return result;
}


float f32_demote_truncate(double input)
{
  #ifndef FE_TOWARDZERO
    #error "FE_TOWARDZERO not supported"
  #endif

  const int originalRoundingMode = fegetround();
  const int didItWork = fesetround( FE_TOWARDZERO );

  if ( 0 == didItWork )
  {
    // it worked
  }
  else
  {
    throw "could not set rounding mode to FE_TOWARDZERO";
  }

  const float result = static_cast<float>(input);

  fesetround( originalRoundingMode );

  return result;
}



uint32_t i32_sign_bit_f64(double input)
{
  const uint32_t result = std::signbit(input);
  return result;
}
uint32_t i32_sign_bit_f32(float input)
{
  const uint32_t result = i32_sign_bit_f64(input);
  return result;
}

int32_t i32_arithmic_signum_f64(double input)
{
  return (double(0) < input) - (input < double(0));
}
int32_t i32_arithmic_signum_f32(float input)
{
  return i32_arithmic_signum_f64(input);
}



