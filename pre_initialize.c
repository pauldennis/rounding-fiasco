// taken from: https://gitlab.haskell.org/ghc/ghc-wasm-meta

#include <stdlib.h>

// Including this since we need access to GHC's RTS API. And it
// transitively includes pretty much all of libc headers that we need.
#include <Rts.h>

// When GHC compiles the Test module with foreign export, it'll
// generate Test_stub.h that declares the prototypes for C functions
// that wrap the corresponding Haskell functions.
#include "RoundingFiasco_stub.h"

// https://stackoverflow.com/questions/1382051/what-is-the-c-equivalent-for-reinterpret-cast
#define REINTERPRET(new_type, var) ( *( (new_type *) &var ) )
float
reinterpret_uint64_to_float(uint64_t i)
{
  uint32_t j = i;
  return REINTERPRET(float, j);
}
double
reinterpret_uint64_to_double(uint64_t i)
{
  return REINTERPRET(double, i);
}

// The prototype of hs_init_with_rtsopts is "void
// hs_init_with_rtsopts(int *argc, char **argv[])" which is a bit
// cumbersome to work with, hence this convenience wrapper.
STATIC_INLINE void hs_init_with_rtsopts_(char *argv[]) {
  int argc;
  for (argc = 0; argv[argc] != NULL; ++argc) {
  }
  hs_init_with_rtsopts(&argc, &argv);
}

// Export this function as "wizer.initialize". wizer also accepts
// "--init-func <init-func>" if you dislike this export name, or
// prefer to pass -Wl,--export=my_init at link-time.
//
// By the time this function is called, the WASI reactor _initialize
// has already been called by wizer. The export entries of this
// function and _initialize will both be stripped by wizer.
__attribute__((export_name("wizer.initialize"))) void __wizer_initialize(void) {
  // The first argument is what you get in getProgName.
  //
  // --nonmoving-gc is recommended when compiling to WASI reactors,
  // since you're likely more concerned about GC pause time than the
  // overall throughput.
  //
  // -H64m sets the "suggested heap size" to 64MB and reserves so much
  // memory when doing GC for the first time. It's not a hard limit,
  // the RTS is perfectly capable of growing the heap beyond it, but
  // it's still recommended to reserve a reasonably sized heap in the
  // beginning. And it doesn't add 64MB to the wizer output, most of
  // the grown memory will be zero anyway!
  char *argv[] = {"", "+RTS", "--nonmoving-gc", "-H2000m", "-RTS", NULL};

  // The WASI reactor _initialize function only takes care of
  // initializing the libc state. The GHC RTS needs to be initialized
  // using one of hs_init* functions before doing any Haskell
  // computation.
  hs_init_with_rtsopts_(argv);

  // Not interesting, I know. The point is you can perform any Haskell
  // computation here! Or C/C++, whatever.

  // outputInterfaceVariations();
  // debug();
  // testsuite();

  // from bugs
  // f32_demote_f64(2147483648, 9223372036854775808);

  float result = f32_convert_i64_unsigned_ceil(reinterpret_uint64_to_double(9223372036854775808u));
  // exit (result);

  result = f32_add_floor
    ( reinterpret_uint64_to_float(0)
    , reinterpret_uint64_to_float(2139095040)
    );

  // exit (result);

  f32_divide_floor
    ( reinterpret_uint64_to_float(2139095039)
    , reinterpret_uint64_to_float(2147483648)
    );
  f32_divide_floor
    ( reinterpret_uint64_to_float(2139095039)
    , reinterpret_uint64_to_float(8388608)
    );
  f32_add_truncate
    ( reinterpret_uint64_to_float(8388608)
    , reinterpret_uint64_to_float(2139095039)
    );

  f32_squareRoot_truncate(reinterpret_uint64_to_float(1));

  f32_add_floor(3.4028235e38, 3.4028235e38);
  f32_add_floor(-3.4028235e38, 3.4028235e38);
  f32_add_floor(-3.4028235e38, -3.4028235e38);
  f32_add_ceil(3.4028235e38, 3.4028235e38);
  f32_add_ceil(-3.4028235e38, 3.4028235e38);
  f32_add_ceil(-3.4028235e38, -3.4028235e38);
  f32_add_truncate(3.4028235e38, 3.4028235e38);
  f32_add_truncate(-3.4028235e38, 3.4028235e38);
  f32_add_truncate(-3.4028235e38, -3.4028235e38);

  f32_squareRoot_ceil(0.0);
  f32_squareRoot_floor(0.0);
  f32_squareRoot_truncate(0.0);
  f32_squareRoot_ceil(1.0);
  f32_squareRoot_floor(1.0);
  f32_squareRoot_truncate(1.0);
  f32_squareRoot_ceil(1.0e-45);
  f32_squareRoot_floor(1.0e-45);
  f32_squareRoot_truncate(1.0e-45);
  f32_squareRoot_ceil(3.4028235e38);
  f32_squareRoot_floor(3.4028235e38);
  f32_squareRoot_truncate(3.4028235e38);

  f32_add_floor(1.0e-45, 1);
  f32_add_floor(5.0e-324,1);


  f32_add_truncate(1.0,1.0e-45);
  f32_add_truncate(-1.0,1.0e-45);
  f64_multiplicate_floor(5.0e-324,1);
  f64_multiplicate_ceil(5.0e-324,1);
  f32_divide_ceil(1, 0.1);
  f32_divide_floor(1, 0.1);

  // Perform a major GC to clean up the heap. When using the nonmoving
  // garbage collector, it's necessary to call it twice to actually
  // free the unused segments.
  hs_perform_gc();
  hs_perform_gc();

  // Finally, zero out the unused RTS memory, to prevent the garbage
  // bytes from being snapshotted into the final wasm module.
  // Otherwise it wouldn't affect correctness, but the wasm module
  // size would bloat significantly. It's only safe to call this after
  // hs_perform_gc() has returned.
  rts_clearMemory();
}

