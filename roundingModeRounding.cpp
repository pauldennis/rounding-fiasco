#pragma STDC FENV_ACCESS ON

#include <iostream>
#include <cstdint>
#include <fenv.h>
#include <math.h>

#include <iomanip>

#include "instructionsUsingRoundingMode.cpp"

#pragma STDC FENV_ACCESS ON


int32_t reinterpret_float_to_int(float i)
{
  return *reinterpret_cast<int32_t*>(&i);
}

uint64_t reinterpret_double_to_uint64_t(double i)
{
  return *reinterpret_cast<uint64_t*>(&i);
}

uint64_t reinterpret_float_to_uint64_t(float i)
{
  uint32_t result = *reinterpret_cast<uint64_t*>(&i);
  return result;
}

double reinterpret_uint64_to_double(uint64_t i)
{
  return *reinterpret_cast<double*>(&i);
}

float reinterpret_uint64_to_float(uint64_t i)
{
  return *reinterpret_cast<float*>(&i);
}

int main()
{
  // std::cout << reinterpret_float_to_int(1) << "\n";
  // std::cout << reinterpret_float_to_int(1.0e-45) << "\n";

  // std::cout << reinterpret_float_to_int(f32_add_floor(1, 1.0e-45)) << "\n";

  // uint64_t i = 9007199254740992 +1+1+1;
  // uint64_t i = 9007199254740995;
  // double i = 5.0e-324;
  // double i = reinterpret_uint64_to_double(2);
  // double i = reinterpret_uint64_to_double(3931642474694443008+1);
  double a_double;
  double b_double;
  a_double = reinterpret_uint64_to_double(4607182418800017408u);
  b_double = reinterpret_uint64_to_double(4613937818241073152u);
  // a = reinterpret_uint64_to_double(18446744073709551615u);
  // b = reinterpret_uint64_to_float(1);
  // a = 10;
  // b = 10;

  float a_float;
  float b_float;
  a_float = reinterpret_uint64_to_float(3212836864u);
  // b_float = ;

  uint64_t ones_64 = 18446744073709551615u;

  // demote: static_cast<float>
  // convert: static_cast<float>

  std::cout << std::setprecision(1000000);
  std::cout << "input a: " << reinterpret_float_to_uint64_t(a_float) << std::endl;
  // std::cout << "input b: " << reinterpret_float_to_uint64_t(b_float) << std::endl;

  std::fesetround(FE_TONEAREST);
  std::cout << "normal: ";
  // std::cout << i << " :  ";
  std::cout << reinterpret_float_to_uint64_t(std::sqrt(a_float)) << std::endl;

  std::fesetround(FE_UPWARD);
  std::cout << "ceil: ";
  // std::cout << i << " :  ";
  std::cout << reinterpret_float_to_uint64_t(std::sqrt(a_float)) << std::endl;

  std::fesetround(FE_DOWNWARD);
  std::cout << "floor : ";
  // std::cout << i << " :  ";
  std::cout << reinterpret_float_to_uint64_t(std::sqrt(a_float)) << std::endl;

  std::fesetround(FE_TOWARDZERO);
  std::cout << "truncate  : ";
  // std::cout << i << " :  ";
  std::cout << reinterpret_float_to_uint64_t(std::sqrt(a_float)) << std::endl;

  return 0;
}


