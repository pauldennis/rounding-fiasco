set -e

wasmtime RoundingFiasco.wasm --invoke f32_add_floor 1 1065353216 # 1.0e-45 + 1
# wasmtime RoundingFiasco.wasm --invoke f32_add_floor 2139095040 4286578688 # +inf + -inf

wasmtime RoundingFiasco.wasm --invoke f32_multiplicate_floor 1 1065353216 # 1.0e-45 1

wasmtime RoundingFiasco.wasm --invoke f32_multiplicate_floor 1315859240 1315859240
wasmtime RoundingFiasco.wasm --invoke f32_multiplicate_ceil 1315859240 1315859240

wasmtime RoundingFiasco.wasm --invoke f32_multiplicate_floor 2139095040 2139095039
wasmtime RoundingFiasco.wasm --invoke f32_multiplicate_ceil 2139095040 4286578688

wasmtime RoundingFiasco.wasm --invoke f32_multiplicate_floor 2139095039 2139095039
wasmtime RoundingFiasco.wasm --invoke f32_multiplicate_ceil 2139095039 2139095039


wasmtime RoundingFiasco.wasm --invoke f32_convert_i64_signed_floor 9223372036854775807
wasmtime RoundingFiasco.wasm --invoke f32_convert_i64_signed_ceil 9223372036854775807
wasmtime RoundingFiasco.wasm --invoke f64_convert_i64_unsigned_floor 9007199254740995







source ./instruction_mapping.sh


wasmtime RoundingFiasco.wasm --invoke testcase $f32_sqrt_ceil 0 0 0
wasmtime RoundingFiasco.wasm --invoke testcase $f32_sqrt_ceil 439682292 1 0
wasmtime RoundingFiasco.wasm --invoke testcase $f64_div_ceil 4599676419421066582 4607182418800017408 4613937818241073152


# https://stackoverflow.com/questions/19473352/are-there-any-whole-numbers-which-the-double-cannot-represent-within-the-min-max
wasmtime RoundingFiasco.wasm --invoke testcase $f64_convert_i64_u_floor 4845873199050653697 9007199254740995 0

# castDoubleToWord64 ((float2Double $ castWord32ToFloat 1) / 2) == 3931642474694443008
wasmtime RoundingFiasco.wasm --invoke testcase $f32_demote_f64_ceil 1 3931642474694443008 0
wasmtime RoundingFiasco.wasm --invoke testcase $f32_demote_f64_floor 0 3931642474694443008 0

wasmtime RoundingFiasco.wasm --invoke testcase $f32_demote_f64_ceil 1 1 0
wasmtime RoundingFiasco.wasm --invoke testcase $f32_demote_f64_floor 0 1 0


echo -e
echo "all tests passed"
